# Detroit - Inventory System

Backend system created for controlling and managing a restaurant inventory.

In this system it is possible to register products, categories, suppliers, employees and metric units; register each product entry to the inventory along with price, date and supplier information and register when a worker requests for an item.

The system provides a Report Module which can extract information of the total value on the inventory based on the price of each product and it’s current amount. This module can also create a shopping list based on the products that are under the required amount.

Users can have one of the three following roles: Admin, Editor and Viewer. Viewers are allowed only to see the information on the system, but can not create or edit them. Editors are allowed to view, create and edit information. Admins have the same privileges of Editors but they are also allowed to create new users.

## Project Structure

### Entry Point

File: `src/server.js`

On this file the connection to the database is made and if it is successful the Express server is started.

### Express App

File: `src/apps/MainApp.js`

Creates the Main Express app and attachments all Routes and middlewares.

### Routes

Folder: `src/routers`

Contains all Routes used by the Main App.

### Validations

Folder: `src/validations`

Contains all Validations used by each Router. All route validations are created using the Joi package.

### Controllers

Folder: `src/controllers`

Contains all Controllers used by each Route. The is also a generic Controller class (`ControllerDao`) which provides basic API Functions for any Database Model, such as: GET by ID, GET All, Post and Put. All other controllers extend this class except for the ones which do not handle a specific Database Table like ControllerAuth and ControllerReport.

### Daos

Folder: `src/dao`

Contains all Data Access Objects for each Database Model. The is also a generic Dao class  (`DaoGeneric`) which is extended by all others and provides basic query methods, such as: find all, find one by primary key, find one by attribute, find all by attribute, create, update, delete, etc.

### Models

Folder: `src/models`

Contains the Models of each database table. All Models were created using Sequelize ORM.

### Auth

Folder: `src/auth`

Contains Classes for handling JWT Authentication.

### Constants

Folder: `src/constants`

Defines constants used in the project.

### Errors

Folder: `src/errors`

Contains maps to all Errors that can be returned by the API

### Utils

Folder: `src/utils`

Helper functions used on the project.

### Tests

Folter: `__tests__`

Contains all tests needed to validate each feature of the API. In order to run tests simply create a file named `test.env` inside `__tests__.setup` folder and run `npm test`. The .env files must specify the following variables:

1. PORT
    - Port the application will run.
2. JWT_SECRET
    - Secret used to sign each Json Web Token used for authentication.
3. JWT_EXP_MIN
    - Expiration time in minutes of the Json Web Token.
4. DATABASE_URL
    - Database Connection URL
5. DB_DIALECT
    - Type of Database being used “postgres” or “mysql”.
6. DB_LOG
    - Defines if each database query should be logged to the console. "TRUE” or “FALSE”.
7. TEARDOWN
    - Defines if the database tables should be truncated after each test file has completed. This variable must be set to “TRUE”  if  more than one test file is running, otherwise one test will interfere with the others. This should be set to “FALSE” only when running a single test file. This way on the end of the test the database won't reset and it is possible to better understand how the test was carried out and how it changed the database state. Very useful for debugging.
