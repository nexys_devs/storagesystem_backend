const JWT = require('jsonwebtoken');

const { JWT_SECRET, JWT_EXP_MIN } = process.env;

class JWTManager {
  static _createPayload(data) {
    return {
      data,
      exp: Date.now() + (60 * (JWT_EXP_MIN || 60))
    };
  }

  static sign(data) {
    return new Promise((resolve, reject) => {
      JWT.sign(JWTManager._createPayload(data), JWT_SECRET, (err, token) => {
        err ? reject(err) : resolve(token);
      });
    });
  }

  static verify(jwt) {
    return new Promise((resolve, reject) => {
      JWT.verify(jwt, JWT_SECRET, (err, decoded) => {
        err ? reject(err) : resolve(decoded);
      });
    });
  }
}

module.exports = JWTManager;