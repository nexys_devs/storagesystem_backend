module.exports = {
  USER_ROLE: {
    ADMIN: 1,
    EDITOR: 2,
    VIEWER: 3,
  }
};