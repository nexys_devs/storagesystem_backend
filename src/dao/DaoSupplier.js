const { Op } = require('sequelize');
const Models = require('./../models');
const DaoGeneric = require('./DaoGeneric');

class DaoSupplier extends DaoGeneric {
  constructor() {
    super(Models.Supplier);
  }

  findAllSimple() {
    return this.findAll({
      attributes: ['name', 'supplier_id'],
      order: [['name']],
    });
  }

  findByQuery(query, options = {}) {
    const { name } = query;

    let _name = { [Op.like]: name || '%' };

    return this.findAll({
      where: { name: _name },
      ...options
    });
  }

  create(obj, options = {}) {
    return super.create({
      name: obj.name,
      company: obj.company,
      email: obj.email,
      phone: obj.phone,
      address: obj.address,
      description: obj.description,
    }, options);
  }

  updateByPk(pk, obj, options = {}) {
    return super.update({
      name: obj.name,
      company: obj.company,
      email: obj.email,
      phone: obj.phone,
      address: obj.address,
      description: obj.description,
    }, {
      ...options,
      where: { supplier_id: pk }
    });
  }
}

module.exports = DaoSupplier;