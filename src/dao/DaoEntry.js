const { Op } = require("sequelize");
const Models = require('./../models');
const DaoGeneric = require('./DaoGeneric');
const DaoRegistry = require('./DaoRegistry');

class DaoEntry extends DaoGeneric {
  constructor() {
    super(Models.Entry);
    this._daoRegistry = new DaoRegistry();

    this._includeSupplier = { model: Models.Supplier, as: 'supplier' };
    this._includeRegistry = { model: Models.Registry, as: 'registry' };
    this._includeProduct = {
      model: Models.Registry,
      as: 'registry',
      include: [{
        model: Models.Product, as: 'product',
      }],
    };

    this._orderByRegistry = {
      order: [[this.model.associations.registry, 'timestamp', 'DESC']]
    };
  }

  findByPkIncludeRegistry(pk) {
    return this.findByPk(pk, {
      include: [this._includeRegistry],
      ...this._orderByRegistry
    });
  }

  findByPkComplete(pk) {
    return this.findByPk(pk, {
      include: [
        this._includeRegistry,
        this._includeSupplier
      ],
      ...this._orderByRegistry
    });
  }

  findByPkIncludeProduct(pk) {
    return this.findByPk(pk, {
      include: [
        this._includeProduct
      ],
      ...this._orderByRegistry
    });
  }

  findAllIncludeRegistry(options = {}) {
    return this.findAll({
      include: [this._includeRegistry],
      ...this._orderByRegistry,
      ...options
    });
  }

  findAllIncludeProduct(options = {}) {
    return this.findAll({
      include: [this._includeProduct],
      ...this._orderByRegistry,
      ...options
    });
  }

  findAllBySupplier(id, options = {}) {
    return this.findAllByProp('supplier_id', id, {
      include: [
        this._includeProduct,
        this._includeSupplier,
      ],
      ...this._orderByRegistry
    }, options);
  }

  findAllByProduct(id, options = {}) {
    return this._findByRegistryProp('product_id', id, options);
  }

  findAllByUser(id, options = {}) {
    return this._findByRegistryProp('user_id', id, options);
  }

  _findByRegistryProp(prop, val, options = {}) {
    return this.findAll({
      ...options,
      include: [
        { ...this._includeProduct, where: { [prop]: val } },
        this._includeSupplier
      ],
      ...this._orderByRegistry
    });
  }

  async create(obj, options = {}) {
    return this.model.create({
      price: obj.price,
      supplier_id: obj.supplier_id,
      registry: {
        amount: obj.amount,
        product_id: obj.product_id,
        user_id: obj.user_id
      }
    }, {
      include: this._includeRegistry,
      ...options
    });
  }

  async updateByPk(pk, obj, options = {}) {
    let transaction = options.transaction;

    await this._daoRegistry.updateByPk(pk, obj, { transaction });

    let entry = await super.update({
      price: obj.price,
      supplier_id: obj.supplier_id
    }, {
      ...options,
      transaction,
      where: { entry_id: pk }
    });

    return entry;
  }

  async filter(filters, options = {}) {
    const {
      priceMin, priceMax, amountMin, amountMax, supplierId, productId, productName,
      timeMin, timeMax
    } = filters;

    //:: Where Entry
    let whereEntry = {}, whereRegistry = {}, whereProduct = {};

    if (supplierId) { whereEntry.supplier_id = supplierId; }

    whereEntry.price = {
      [Op.between]: [
        priceMin ? priceMin : 0,
        priceMax ? priceMax : 1000000000
      ]
    };

    //:: Where Registry
    whereRegistry.amount = {
      [Op.between]: [
        amountMin ? amountMin : 0,
        amountMax ? amountMax : 1000000000
      ]
    };

    whereRegistry.timestamp = {
      [Op.between]: [
        timeMin ? timeMin : '1990-01-01',
        timeMax ? timeMax : '9999-01-01'
      ]
    };

    //:: Where Product
    if (productId) { whereProduct.product_id = productId; }
    if (productName) { whereProduct.name = { [Op.like]: productName }; }

    return this.model.findAll({
      where: whereEntry,
      include: [
        {
          model: Models.Registry,
          as: 'registry',
          where: whereRegistry,
          include: [{
            model: Models.Product,
            as: 'product',
            where: whereProduct,
            include: [{
              model: Models.Category,
              as: 'categories'
            }]
          }]
        },
        this._includeSupplier
      ],
      ...options
    });
  }

  async findLatestEntries() {
    return this.model.findAll({
      include: [this._includeRegistry, this._includeProduct],
      ...this._orderByRegistry
    });
  }
}

module.exports = DaoEntry;