const { Op } = require('sequelize');
const Models = require('../models');
const DaoGeneric = require('./DaoGeneric');

class DaoStorage extends DaoGeneric {
  constructor() {
    super(Models.Storage);
  }

  findAllSimple() {
    return this.findAll({
      attributes: ['name', 'storage_id'],
      order: [['name']],
    });
  }

  findByQuery(query, options = {}) {
    const { name } = query;

    let _name = { [Op.like]: name || '%' };

    return this.findAll({
      where: { name: _name },
      ...options
    });
  }

  create(obj, options = {}) {
    return super.create({
      name: obj.name,
      description: obj.description
    }, options);
  }

  updateByPk(pk, obj, options = {}) {
    return super.update({
      name: obj.name,
      description: obj.description
    }, {
      ...options,
      where: { storage_id: pk }
    });
  }
}

module.exports = DaoStorage;