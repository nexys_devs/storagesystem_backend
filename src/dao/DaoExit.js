const { Op } = require("sequelize");
const Models = require('./../models');
const DaoGeneric = require('./DaoGeneric');
const DaoRegistry = require('./DaoRegistry');

class DaoExit extends DaoGeneric {
  constructor() {
    super(Models.Exit);
    this._daoRegistry = new DaoRegistry();

    this._includeRegistry = { model: Models.Registry, as: 'registry' };
    this._includeProduct = {
      model: Models.Registry,
      as: 'registry',
      include: [{
        model: Models.Product, as: 'product'
      }]
    };

    this._includeWorker = { model: Models.Worker, as: 'worker' };
    this._includeStorage = { model: Models.Storage, as: 'storage' };

    this._orderByRegistry = {
      order: [[this.model.associations.registry, 'timestamp', 'DESC']]
    };
  }

  findByPkIncludeRegistry(pk) {
    return this.findByPk(pk, {
      include: [this._includeRegistry],
      ...this._orderByRegistry
    });
  }

  findByPkComplete(pk) {
    return super.findByPk(pk, {
      include: [
        this._includeRegistry,
        this._includeWorker,
        this._includeStorage
      ],
      ...this._orderByRegistry
    });
  }

  findByPkIncludeProduct(pk) {
    return super.findByPk(pk, {
      include: [
        this._includeProduct,
        this._includeWorker,
        this._includeStorage
      ],
      ...this._orderByRegistry
    });
  }

  findAllIncludeRegistry(options = {}) {
    return this.findAll({
      include: [this._includeRegistry],
      ...this._orderByRegistry,
      ...options
    });
  }

  findAllIncludeProduct(options = {}) {
    return this.findAll({
      include: [this._includeProduct],
      ...this._orderByRegistry,
      ...options
    });
  }

  findAllByWorker(id, options = {}) {
    return this.findAll({
      where: { worker_id: id },
      include: [
        this._includeProduct, this._includeWorker, this._includeStorage
      ],
      ...this._orderByRegistry,
      ...options
    });
  }

  findAllByStorage(id, options = {}) {
    return this.findAll({
      where: { storage_id: id },
      include: [
        this._includeProduct, this._includeWorker, this._includeStorage
      ],
      ...this._orderByRegistry,
      ...options
    });
  }

  findAllByProduct(id, options = {}) {
    return this._findByRegistryProp('product_id', id, options);
  }

  findAllByUser(id, options = {}) {
    return this._findByRegistryProp('user_id', id, options);
  }

  _findByRegistryProp(prop, val, options = {}) {
    return this.findAll({
      ...options,
      include: [
        { ...this._includeProduct, where: { [prop]: val } },
        this._includeWorker,
        this._includeStorage
      ],
      ...this._orderByRegistry
    });
  }

  async create(obj, options = {}) {
    return this.model.create({
      worker_id: obj.worker_id,
      storage_id: obj.storage_id,
      registry: {
        amount: obj.amount,
        product_id: obj.product_id,
        user_id: obj.user_id
      }
    }, {
      include: this._includeRegistry,
      ...options
    });
  }

  async updateByPk(pk, obj, options = {}) {
    let transaction = options.transaction;

    await this._daoRegistry.updateByPk(pk, obj, { transaction });

    let exit = await super.update({
      worker_id: obj.worker_id,
      storage_id: obj.storage_id
    }, {
      ...options,
      transaction,
      where: { exit_id: pk }
    });

    return exit;
  }

  async filter(filters, options = {}) {
    const {
      amountMin, amountMax, timeMin, timeMax, productId, productName,
      storageId, storageName, workerId, workerName
    } = filters;

    // Queries
    let whereExit = {}, whereRegistry = {}, whereProduct = {}, whereWorker = {}, whereStorage = {};

    // Exit
    if (workerId) { whereExit.worker_id = workerId; }
    if (storageId) { whereExit.storage_id = storageId; }

    // Registry
    whereRegistry.amount = {
      [Op.between]: [
        amountMin ? amountMin : 0,
        amountMax ? amountMax : 1000000000
      ]
    };

    whereRegistry.timestamp = {
      [Op.between]: [
        timeMin ? timeMin : '1990-01-01',
        timeMax ? timeMax : '9999-01-01'
      ]
    };

    //:: Where Product
    if (productId) { whereProduct.product_id = productId; }
    if (productName) { whereProduct.name = { [Op.like]: productName }; }

    // Storage
    if (storageName) { whereStorage.name = { [Op.like]: storageName }; }

    // Worker
    if (workerName) { whereWorker.name = { [Op.like]: workerName }; }

    return this.model.findAll({
      where: whereExit,
      include: [
        {
          model: Models.Registry,
          as: 'registry',
          where: whereRegistry,
          include: [{
            model: Models.Product,
            as: 'product',
            where: whereProduct,
            include: [{ model: Models.Category, as: 'categories' }]
          }]
        },
        { model: Models.Worker, as: 'worker', where: whereWorker },
        { model: Models.Storage, as: 'storage', where: whereStorage }
      ],
      ...options
    });
  }
}

module.exports = DaoExit;