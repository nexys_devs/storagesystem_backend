class DaoGeneric {
  constructor(model) {
    this.model = model;
  }

  findByPk(pk, options = {}) {
    return this.model.findByPk(pk, options);
  }

  findOne(options = {}) {
    return this.model.findOne(options);
  }

  findOneByProp(prop, val) {
    return this.model.findOne({ where: { [prop]: val } });
  }

  findAll(options = {}) {
    return this.model.findAll(options);
  }

  findAllByProp(prop, val, options) {
    return this.model.findAll({ where: { [prop]: val }, ...options });
  }

  create(obj, option = {}) {
    return this.model.create(obj, option);
  }

  update(obj, options) {
    return this.model.update(obj, options);
  }

  updateByPk(_pk, _obj, _option) { }

  destroy(options) {
    return this.model.destroy(options);
  }

  startTransaction() {
    return this.model.sequelize.transaction();
  }

  managedTransaction(callback) {
    return this.model.sequelize.transaction(t => callback(t));
  }

  _addPropToQuery(query, prop, val) {
    if (val !== undefined)
      query[prop] = val;

    return query;
  }
}

module.exports = DaoGeneric;