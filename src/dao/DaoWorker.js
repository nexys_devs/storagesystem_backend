const { Op } = require('sequelize');
const Models = require('./../models');
const DaoGeneric = require('./DaoGeneric');

class DaoWorker extends DaoGeneric {
  constructor() {
    super(Models.Worker);
  }

  findAllSimple() {
    return this.findAll({
      attributes: ['name', 'worker_id'],
      order: [['name']],
    });
  }

  findByQuery(query, options = {}) {
    const { name } = query;

    let _name = { [Op.like]: name || '%' };

    return this.findAll({
      where: { name: _name },
      ...options
    });
  }

  create(obj, options = {}) {
    return super.create({
      name: obj.name
    }, options);
  }

  updateByPk(pk, obj, options = {}) {
    return super.update({
      name: obj.name,
    }, {
      ...options,
      where: { worker_id: pk }
    });
  }
}

module.exports = DaoWorker;