const Models = require('./../models');
const DaoGeneric = require('./DaoGeneric');

class DaoRegistry extends DaoGeneric {
  constructor() {
    super(Models.Registry);
  }

  create(obj, options = {}) {
    return super.create({
      amount: obj.amount,
      product_id: obj.product_id,
      user_id: obj.user_id
    }, options);
  }

  updateByPk(pk, obj, options = { }) {
    return super.update({
      amount: obj.amount,
      product_id: obj.product_id,
      user_id: obj.user_id
    }, {
      where: { registry_id: pk },
      ...options,
    });
  }
}

module.exports = DaoRegistry;