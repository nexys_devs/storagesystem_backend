const { Op, Sequelize } = require('sequelize');
const Models = require('./../models');
const DaoGeneric = require('./DaoGeneric');

class DaoProduct extends DaoGeneric {
  constructor() {
    super(Models.Product);

    this._includeUD = { model: Models.UD, as: 'ud' };
    this._includeCategory = { model: Models.Category, as: 'categories', through: { attributes: [] } };
  }

  findAllSimple() {
    return this.findAll({
      attributes: ['name', 'product_id'],
      order: [['name']],
    });
  }

  findaAllComplete(options = {}) {
    return this.findAll({
      include: [
        this._includeUD,
        this._includeCategory
      ],
      ...options
    });
  }

  findaAllIncludeCategory(options = {}) {
    return this.findAll({
      include: [this._includeCategory],
      ...options
    });
  }

  findaAllIncludeCategoryID(options = {}) {
    return this.findAll({
      include: [
        { ...this._includeCategory, attributes: ['category_id'] }
      ],
      ...options
    });
  }

  findByPkComplete(pk) {
    return this.findByPk(pk, {
      include: [
        this._includeUD,
        this._includeCategory
      ]
    });
  }

  findAllByCategoryComplete(categoryID, options = {}) {
    return this.findAll({
      include: [
        this._includeUD,
        {
          ...this._includeCategory,
          where: { category_id: categoryID }
        }
      ],
      ...options
    });
  }

  findByQuery(query, options = {}) {
    const { name, ...rest } = query;

    let _name = { [Op.like]: name || '%' };

    return this.findAll({
      where: { ...rest, name: _name },
      include: [this._includeUD],
      ...options
    });
  }

  findByQueryComplete(query, options = {}) {
    const { name, ...rest } = query;

    let _name = { [Op.like]: name || '%' };

    return this.findaAllComplete({
      where: { ...rest, name: _name },
      ...options
    });
  }

  create(obj, options = {}) {
    return super.create({
      name: obj.name,
      ud_id: obj.ud_id,
      warning_amount: obj.warning_amount,
      danger_amount: obj.danger_amount,
      current_amount: obj.current_amount,
      description: obj.description,
    }, options);
  }

  updateByPk(pk, obj, options = {}) {
    return this.update({
      name: obj.name,
      ud_id: obj.ud_id,
      warning_amount: obj.warning_amount,
      danger_amount: obj.danger_amount,
      current_amount: obj.current_amount,
      description: obj.description,
    }, {
      ...options,
      where: { product_id: pk }
    });
  }

  destroy(pId) {
    return super.destroy({
      where: { product_id: pId }
    });
  }

  // Reports
  shopList() {
    return this._shopList({
      current_amount: { [Op.lte]: Sequelize.col('warning_amount') }
    });
  }

  shopListWarning() {
    return this._shopList({
      current_amount: {
        [Op.and]: [
          { [Op.lte]: Sequelize.col('warning_amount') },
          { [Op.gt]: Sequelize.col('danger_amount') }
        ]
      }
    });
  }

  shopListDanger() {
    return this._shopList({
      current_amount: { [Op.lte]: Sequelize.col('danger_amount') }
    });
  }

  _shopList(where = {}) {
    return this.findAll({
      where,
      attributes: ['product_id', 'name', 'warning_amount', 'danger_amount', 'current_amount', 'ud_id'],
      include: [
        {
          ...this._includeUD,
          attributes: ['ud_id', 'code']
        },
        this._includeCategory
      ],
      order: [['name']]
    });
  }
}

module.exports = DaoProduct;
