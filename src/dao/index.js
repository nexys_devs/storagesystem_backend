module.exports = {
  DaoUD: require('./DaoUD'),
  DaoSupplier: require('./DaoSupplier'),
  DaoUser: require('./DaoUser'),
  DaoProduct: require('./DaoProduct'),
  DaoUserAuth: require('./DaoUserAuth'),
  DaoCategory: require('./DaoCategory'),
  DaoStorage: require('./DaoStorage'),
  DaoWorker: require('./DaoWorker'),
  DaoExit: require('./DaoExit'),
  DaoEntry: require('./DaoEntry'),
  DaoProductCategory: require('./DaoProductCategory')
};