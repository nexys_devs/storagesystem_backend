const bcrypt = require('bcryptjs');
const Models = require('./../models');
const DaoGeneric = require('./DaoGeneric');

class DaoUserAuth extends DaoGeneric {
  constructor() {
    super(Models.UserAuth);
  }

  findByUserId(id) {
    return this.findOneByProp('user_id', id);
  }

  create(obj, options = {}) {
    return super.create({
      secret: obj.secret,
      user_id: obj.user_id
    }, options);
  }

  authenticate(psw, secret) {
    return new Promise((resolve, reject) => {
      bcrypt.compare(psw, secret, function (err, ok) {
        err ? reject(err) : resolve(ok);
      });
    });
  }

  findByPk(_, __ = {}) { return false; }
  findAll(_ = {}) { return false; }
  update(_, __) { return false; }
  destroy(_) { return false; }
}

module.exports = DaoUserAuth;