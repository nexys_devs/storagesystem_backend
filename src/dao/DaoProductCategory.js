const Models = require('./../models');
const DaoGeneric = require('./DaoGeneric');

class DaoProductCategory extends DaoGeneric {
  constructor() {
    super(Models.ProductCategory);
  }

  create(obj, options = {}) {
    return super.create({
      product_id: obj.product_id,
      category_id: obj.category_id
    }, options);
  }

  addCategoriesToProduct(pId, cIds, options = {}) {
    return this.model.bulkCreate(
      cIds.map(cId => ({ product_id: pId, category_id: cId })),
      options
    );
  }

  addProductsToCategory(cId, pIds, options = {}) {
    return this.model.bulkCreate(
      pIds.map(pId => ({ category_id: cId, product_id: pId })),
      options
    );
  }

  destroyProductCategories(pId, cIds, options = {}) {
    return this.model.destroy({
      where: { product_id: pId, category_id: cIds }
    }, options);
  }
}

module.exports = DaoProductCategory;