const Models = require('./../models');
const DaoGeneric = require('./DaoGeneric');
const DaoUserAuth = require('./DaoUserAuth');

class DaoUser extends DaoGeneric {
  constructor() {
    super(Models.User);
  }

  findOneByEmail(email) {
    return this.findOneByProp('email', email);
  }

  findOneByName(name) {
    return this.findOneByProp('name', name);
  }

  findOneByLogin(login) {
    return this.findOneByProp('login', login);
  }

  findByPkIncludeRole(pk) {
    return super.findByPk(pk, {
      include: [{
        model: Models.UserRole,
        as: 'role'
      }]
    });
  }

  create(obj, options = {}) {
    return super.create({
      login: obj.login,
      first_name: obj.first_name,
      last_name: obj.last_name,
      email: obj.email,
      phone: obj.phone,
      birth: obj.birth,
      user_role_id: obj.user_role_id
    }, options);
  }

  updateByPk(pk, obj, options = {}) {
    return super.update({
      first_name: obj.first_name,
      last_name: obj.last_name,
      phone: obj.phone,
      birth: obj.birth,
      user_role_id: obj.user_role_id
    }, {
      ...options,
      where: { user_id: pk }
    });
  }

  updateRole(pk, role, options = {}) {
    return super.update({
      user_role_id: role
    }, {
      ...options,
      where: { user_id: pk }
    });
  }

  async authenticate(login, psw) {
    let user = await this.findOneByLogin(login);

    if (!user)
      user = await this.findOneByEmail(login);

    if (!user) { return null; }

    const DaoAuth = new DaoUserAuth();

    let auth = await DaoAuth.findByUserId(user.user_id);
    if (!auth) { return null; }

    let ok = await DaoAuth.authenticate(psw, auth.secret); // TODO - Throw BcryptError

    return ok ? user : null;
  }
}

module.exports = DaoUser;