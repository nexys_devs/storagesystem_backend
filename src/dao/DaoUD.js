const { Op } = require('sequelize');
const Models = require('./../models');
const DaoGeneric = require('./DaoGeneric');

class DaoUD extends DaoGeneric {
  constructor() {
    super(Models.UD);
  }

  findAllSimple() {
    return this.findAll({
      attributes: ['ud_id', 'code'],
      order: [['code']],
    });
  }

  findByQuery(query) {
    const { code, description } = query;

    let _code = { [Op.like]: code || '%' };
    let _description = { [Op.like]: description || '%' };

    return this.findAll({
      where: { code: _code, description: _description }
    });
  }

  create(obj, options = {}) {
    return super.create({
      code: obj.code,
      description: obj.description
    }, options);
  }

  updateByPk(pk, obj, options = {}) {
    return super.update({
      code: obj.code,
      description: obj.description
    }, {
      ...options,
      where: { ud_id: pk }
    });
  }
}

module.exports = DaoUD;