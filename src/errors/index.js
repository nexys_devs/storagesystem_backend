module.exports = {
  ...require('./CustomErrors'),
  Errorhandler: require('./Errorhandler') ,
  SequelizeErrorHandler: require('./SequelizeErrorHandler')
};