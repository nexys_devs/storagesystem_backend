const { ErrorConstraint, ErrorUniqueConstraint } = require('./CustomErrors');

class SequelizeError extends Error {
  constructor(seqErr) {
    Error.stackTraceLimit = 4;
    super(seqErr.name);

    this.originalError = seqErr;
  }
}

module.exports = function (err, _sequelize, _Sequelize) {
  switch (err.name) {
    case 'SequelizeUniqueConstraintError': {
      return new ErrorUniqueConstraint(err.parent.constraint);
    }
    case 'SequelizeForeignKeyConstraintError': {
      return new ErrorConstraint(err.parent.constraint);
    }
    default: return new SequelizeError(err);
  }
};

module.exports.SequelizeError = SequelizeError;