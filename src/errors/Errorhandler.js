const Validate = require('express-validation');
const { CustomError } = require('./CustomErrors');
const { Logger } = require('./../utils');
const { SequelizeError } = require('./SequelizeErrorHandler');

module.exports = function (err, _, res, _next) {
  if (err instanceof CustomError)
    return err.sendRes(res);

  if (err instanceof Validate.ValidationError) {
    Logger.warn('Validation Error', err);
    return res.status(err.status).json(err);
  }

  if (err instanceof SyntaxError) {
    Logger.warn('Syntax Error');
    return res.status(err.status).json(err);
  }

  if (err instanceof SequelizeError) {
    Logger.critical('Sequelize Error', err.originalError);
    return res.status(500).json(err.originalError);
  }

  Logger.error('Critical Error', err);
  return res.status(500).send(err);
};