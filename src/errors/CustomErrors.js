class CustomError extends Error {
  constructor(code, resCode, msg, desc, path) {
    Error.stackTraceLimit = 4;
    super(msg);

    this.__customError = true;
    this.error = code;
    this.status = resCode;
    this.msg = msg;
    this.desc = desc;
    this.path = path;
    this.timestamp = new Date().toISOString();
  }

  sendRes(res) {
    res.status(this.status).send(this);
  }

  toString() {
    return JSON.stringify({
      __customError: this.__customError,
      error: this.error__customError,
      status: this.status__customError,
      msg: this.msg__customError,
      desc: this.desc__customError,
      path: this.path__customError,
      timestamp: this.timestamp,
    });
  }
}

class ErrorBadLogin extends CustomError {
  constructor() {
    super(
      'BadLogin',
      400,
      'Credenciais inválidas', // 'Bad login credentials',
      'Senha ou Login Inválidos', //'Invalid Login or Password.',
    );
  }
}

class ErrorInsufficientAmount extends CustomError {
  constructor(productAmount, productId, amountToRemove, path) {
    super(
      'InsufficientAmount',
      409,
      `Quantidade insuficiente para remover (${amountToRemove}) - Produto ${productId}`,
      `Existem apenas ${productAmount} unidades do produto ${productId}`,
      path
    );
  }
}

class Error404 extends CustomError {
  constructor(msg, path) {
    super(
      'UnknownResource',
      404,
      msg,
      msg,
      path
    );
  }
}

class ErrorConstraint extends CustomError {
  constructor(key) {
    super(
      'ConstraintError',
      400,
      `Constraint Error - Valor inválido para ${key}` // `Constraint Error - Invalid Value for ${key}`
    );
  }
}

class ErrorUniqueConstraint extends CustomError {
  constructor(key) {
    super(
      'UniqueConstraintError',
      400,
      `Constraint Error - Valor duplicado para ${key}` // `UniqueConstraintError - Duplicate value for ${key}`
    );
  }
}

class ErrorInvalidJWT extends CustomError {
  // JWT Error: TokenExpiredError, JsonWebTokenError
  constructor(jwtError, path) {
    if (jwtError.name === 'TokenExpiredError')
      super(
        'TokenExpired',
        401,
        'Token expirado', // 'Token has expired.',
        'Seu Token expirou. Efetue um novo Login.', // 'Your token has expired. Please login again.',
        path
      );
    else
      super(
        'TokenInvalid',
        401,
        'Token enviado invalido.', // 'Invalid token was provided.',
        '',
        path
      );
  }
}

class ErrorNotLoggedIn extends CustomError {
  constructor(path) {
    super(
      'NotLoggedIn',
      401,
      'Usuário não efetuou o Login.', // 'Not Logged In ',
      'Usuário deve efetuar o login para realizar essa ação', // Login before perform this action',
      path
    );
  }
}

class ErrorRoleNotAuthorized extends CustomError {
  constructor(path) {
    super(
      'RoleNotAuthorized',
      401,
      'Permissão não autorizada', // 'Role not Authorized.',
      'Sem permissão suficiente para realizar essa ação', // 'Not enought permissions to perform this action',
      path
    );
  }
}

module.exports = {
  CustomError,
  Error404,
  ErrorBadLogin,
  ErrorInvalidJWT,
  ErrorNotLoggedIn,
  ErrorRoleNotAuthorized,
  ErrorConstraint,
  ErrorInsufficientAmount,
  ErrorUniqueConstraint
};