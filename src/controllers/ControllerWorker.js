const ControllerDao = require('./ControllerDao');
const { DaoWorker } = require('./../dao');

class ControllerWorker extends ControllerDao {
  constructor() {
    super(
      new DaoWorker(),
      'workerId',
      'Worker'
    );
  }

  findAllSimple() {
    return async (_, res, next) => {
      this.dao.findAllSimple()
        .then(r => res.send(r)).catch(next);
    };
  }

  findAll() {
    return async (req, res, next) => {
      this.dao.findAll({ order: ['name'], ...this._getOffset(req) })
        .then(r => res.send(r)).catch(next);
    };
  }

  findByQuery() {
    return async (req, res, next) => {
      this.dao.findByQuery(req.query, { order: ['name'] })
        .then(r => res.send(r))
        .catch(err => next(err));
    };
  }
}

module.exports = ControllerWorker;