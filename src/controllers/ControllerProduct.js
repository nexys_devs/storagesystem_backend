const ControllerDao = require('./ControllerDao');
const { DaoProduct, DaoProductCategory, DaoCategory } = require('./../dao');

class ControllerProduct extends ControllerDao {
  constructor() {
    super(
      new DaoProduct(),
      'productId',
      'Product'
    );

    this.daoProductCategory = new DaoProductCategory();
    this.daoCategory = new DaoCategory();
  }

  findByPk() {
    return this._findByPk(id => this.dao.findByPkComplete(id));
  }

  findAllSimple() {
    return async (_, res, next) => {
      this.dao.findAllSimple()
        .then(r => res.send(r)).catch(next);
    };
  }

  findAllComplete() {
    return async (req, res, next) => {
      this.dao.findaAllComplete({ order: ['name'], ...this._getOffset(req) })
        .then(r => res.send(r)).catch(next);
    };
  }

  findAllIncludeCategory() {
    return async (req, res, next) => {
      this.dao.findaAllIncludeCategory({ order: ['name'], ...this._getOffset(req) })
        .then(r => res.send(r)).catch(next);
    };
  }

  findByQuery() {
    return async (req, res, next) => {
      this.dao.findByQueryComplete(req.query, { order: ['name'] })
        .then(r => res.send(r))
        .catch(err => next(err));
    };
  }

  create() {
    return async (req, res, next) => {
      let transaction;
      const { categories, ...prod } = req.body;

      try {
        transaction = await this.dao.startTransaction();

        const { product_id } = await this.dao.create({ ...prod, current_amount: 0 }, { transaction });

        if (categories && categories.length)
          await this.daoProductCategory.addCategoriesToProduct(product_id, categories, { transaction });

        await transaction.commit();

        let p = await this.dao.findByPkComplete(product_id);
        res.send(p);
      } catch (err) {
        if (transaction) { await transaction.rollback(); }
        next(err);
      }
    };
  }

  update() {
    return async (req, res, next) => {
      let transaction;
      const ID = this._getIdKey(req);
      const { categories, current_amount: _, ...prod } = req.body;

      try {
        let p = await this.dao.findByPkComplete(ID);
        if (!p) { throw this._UnknownError(req.path); }

        transaction = await this.dao.startTransaction();
        await this.dao.updateByPk(ID, prod, { transaction });

        await this.__setProductCategories(p, categories, { transaction });

        await transaction.commit();
        res.sendStatus(204);
      } catch (err) {
        if (transaction) { await transaction.rollback(); }
        next(err);
      }
    };
  }

  addProductCategory() {
    return async (req, res, next) => {
      try {
        const ID = this._getIdKey(req);

        let p = await this.dao.findByPkComplete(ID);
        if (!p) { throw this._UnknownError(req.path); }

        let pcIds = p.categories.map(c => c.category_id);
        let _ids = req.body.filter(i => !pcIds.includes(i));

        if (_ids.length)
          await this.daoProductCategory.addCategoriesToProduct(ID, _ids);

        res.sendStatus(204);
      } catch (err) {
        next(err);
      }
    };
  }

  removeProductCategory() {
    return async (req, res, next) => {
      try {
        const ID = this._getIdKey(req);

        let p = await this.dao.findByPkComplete(ID);
        if (!p) { throw this._UnknownError(req.path); }

        let pcIds = p.categories.map(c => c.category_id);
        let _ids = req.body.filter(i => pcIds.includes(i));

        if (_ids.length)
          await this.daoProductCategory.destroyProductCategories(ID, _ids);

        res.sendStatus(204);
      } catch (err) {
        next(err);
      }
    };
  }

  setProductCategory() {
    return async (req, res, next) => {
      let transaction;

      try {
        const ID = this._getIdKey(req);

        let p = await this.dao.findByPkComplete(ID);
        if (!p) { throw this._UnknownError(req.path); }

        transaction = await this.dao.startTransaction();

        await this.__setProductCategories(p, req.body, { transaction });

        transaction.commit();
        res.sendStatus(204);
      } catch (err) {
        if (transaction) { await transaction.rollback(); }
        next(err);
      }
    };
  }

  delete() {
    return async (req, res, next) => {

      try {
        const ID = this._getIdKey(req);

        await this.dao.destroy(ID);

        res.sendStatus(204);
      } catch (err) {
        next(err);
      }
    };
  }

  //:: Helpers
  async __setProductCategories(product, categories, options = {}) {
    if (!categories) { return; }

    let pcIds = product.categories.map(c => c.category_id);

    let _idsToAdd = categories.filter(i => !pcIds.includes(i));
    let _idsToRem = pcIds.filter(i => !categories.includes(i));


    if (_idsToAdd.length)
      await this.daoProductCategory.addCategoriesToProduct(product.product_id, _idsToAdd, options);

    if (_idsToRem.length)
      await this.daoProductCategory.destroyProductCategories(product.product_id, _idsToRem, options);
  }
}

module.exports = ControllerProduct;