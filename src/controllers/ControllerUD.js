const ControllerDao = require('./ControllerDao');
const { DaoUD } = require('./../dao');

class ControllerUD extends ControllerDao {
  constructor() {
    super(
      new DaoUD(),
      'udId',
      'UD'
    );
  }

  findAllSimple() {
    return async (_, res, next) => {
      this.dao.findAllSimple()
        .then(r => res.send(r)).catch(next);
    };
  }

  findByQuery() {
    return async (req, res, next) => {
      this.dao.findByQuery(req.query).then(r => res.send(r)).catch(err => next(err));
    };
  }

}

module.exports = ControllerUD;