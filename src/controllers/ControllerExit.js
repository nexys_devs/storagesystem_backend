const ControllerDao = require('./ControllerDao');
const { round } = require('./../utils');
const { DaoExit, DaoProduct } = require('./../dao');
const { Error404, ErrorInsufficientAmount } = require('./../errors');

class ControllerExit extends ControllerDao {
  constructor() {
    super(
      new DaoExit(),
      'exitId',
      'Exit'
    );

    this._daoProduct = new DaoProduct();
  }

  findByPk() {
    return this._findByPk(id => this.dao.findByPkComplete(id));
  }

  findByPkIncludeRegistry() {
    return this._findByPk(id => this.dao.findByPkIncludeRegistry(id));
  }

  findByPkIncludeProduct() {
    return this._findByPk(id => this.dao.findByPkIncludeProduct(id));
  }

  findAllIncludeRegistry() {
    return async (_, res, next) => {
      this.dao.findAllIncludeRegistry().then(r => res.send(r)).catch(next);
    };
  }

  findAllIncludeProduct() {
    return async (req, res, next) => {
      this.dao.findAllIncludeProduct(this._getOffset(req))
        .then(r => res.send(r)).catch(next);
    };
  }

  findAllByWorker() {
    return this._findAllBy('workerId', this.dao.findAllByWorker);
  }

  findAllByStorage() {
    return this._findAllBy('storageId', this.dao.findAllByStorage);
  }

  findAllByProduct() {
    return this._findAllBy('productId', this.dao.findAllByProduct);
  }

  findAllByUser() {
    return this._findAllBy('userId', this.dao.findAllByUser);
  }

  _findAllBy(key, daoCall) {
    return async (req, res, next) => {
      const { [key]: ID } = req.params;
      if (!ID) { return next(this._UnknownError(req.path)); }

      daoCall.call(this.dao, ID).then(r => res.send(r)).catch(next);
    };
  }

  create() {
    return async (req, res, next) => {
      let transaction;

      try {
        let p = await this._daoProduct.findByPk(req.body.product_id);
        if (!p) { throw new Error404(`Produto desconhecido com id ${req.body.product_id}`, req.path); }

        transaction = await this.dao.startTransaction();

        let newAmount = round(p.current_amount - req.body.amount);

        if (newAmount < 0)
          throw new ErrorInsufficientAmount(p.current_amount, p.product_id, req.body.amount, req.path);

        await this._daoProduct.updateByPk(p.product_id, { current_amount: newAmount }, { transaction });

        let e = await this.dao.create({
          ...req.body, user_id: req.user.user_id
        }, { transaction });

        await transaction.commit();

        res.send(e);
      } catch (err) {
        if (transaction) { await transaction.rollback(); }

        next(err);
      }
    };
  }

  update() {
    return async (req, res, next) => {
      let transaction;
      const ID = this._getIdKey(req);

      try {
        let e = await this.dao.findByPkIncludeProduct(ID);
        if (!e) { throw this._UnknownError(req.path); }

        transaction = await this.dao.startTransaction();

        // Same Product
        if (!req.body.product_id || e.registry.product.product_id === req.body.product_id) {

          let newAmount = e.registry.product.current_amount + e.registry.amount - req.body.amount;

          if (newAmount < 0) {
            throw new ErrorInsufficientAmount(
              e.registry.product.current_amount, e.registry.product.product_id,
              req.body.amount, req.path
            );
          }

          await this._daoProduct.updateByPk(e.registry.product.product_id,
            { current_amount: round(newAmount) }, { transaction }
          );

        } else {
          // Different Prodcut ID
          let newProduct = await this._daoProduct.findByPk(req.body.product_id);
          let newAmount = newProduct.current_amount - req.body.amount;

          if (newAmount < 0) {
            throw new ErrorInsufficientAmount(
              e.registry.product.current_amount, req.body.product_id,
              req.body.amount, req.path
            );
          }

          // Update New Product
          await this._daoProduct.updateByPk(newProduct.product_id,
            { current_amount: round(newAmount) }, { transaction }
          );

          // Update Old Product
          await this._daoProduct.updateByPk(e.registry.product.product_id, {
            current_amount: round(e.registry.product.current_amount + e.registry.amount)
          }, { transaction });
        }

        await this.dao.updateByPk(ID, req.body, { transaction });

        await transaction.commit();

        res.sendStatus(204);
      } catch (err) {
        if (transaction) { await transaction.rollback(); }

        next(err);
      }
    };
  }

  query() {
    return async (req, res, next) => {
      this.dao.filter(req.query).then(r => res.send(r)).catch(next);
    };
  }
}

module.exports = ControllerExit;