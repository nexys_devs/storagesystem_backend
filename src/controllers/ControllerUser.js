const bcrypt = require('bcryptjs');
const ControllerDao = require('./ControllerDao');
const { DaoUser, DaoUserAuth } = require('./../dao');

class ControllerUser extends ControllerDao {
  constructor() {
    super(
      new DaoUser(),
      'userId',
      'User'
    );

    this._daoUserAuth = new DaoUserAuth();
  }

  create() {
    return async (req, res, next) => {
      let transaction;
      const { password, ...userData } = req.body;

      try {
        transaction = await this._daoUserAuth.startTransaction();

        let user = await this.dao.create(userData, { transaction });

        let hash = await bcrypt.hash(password, 10); // TODO - Throw BcryptError

        await this._daoUserAuth.create({
          user_id: user.user_id,
          secret: hash
        }, { transaction });

        await transaction.commit();

        res.send(user);
      } catch (err) {
        if (transaction) { await transaction.rollback(); }
        next(err);
      }
    };
  }

  setRole() {
    return async (req, res, next) => {
      const { userId, role } = req.params;

      this.dao.updateRole(userId, role)
        .then(_ => res.sendStatus(204))
        .catch(err => next(err));
    };
  }
}

module.exports = ControllerUser;