const Controller = require('./Controller');
const { DaoProduct, DaoEntry, DaoCategory } = require('./../dao');
const Logger = require('../utils/logger/Logger');
const { sort, round } = require('../utils');

class ControllerReport extends Controller {
  constructor() {
    super();

    this._daoProduct = new DaoProduct();
    this._daoEntry = new DaoEntry();
    this._daoCategory = new DaoCategory();
  }

  shoplist() {
    return async (req, res, next) => {
      try {
        let prods;
        const { level } = req.query;

        switch (level.toUpperCase()) {
          case 'WARN': prods = await this._daoProduct.shopListWarning(); break;
          case 'DANGER': prods = await this._daoProduct.shopListDanger(); break;
          default: prods = await this._daoProduct.shopList(); break;
        }

        res.send(prods);
      } catch (err) {
        next(err);
      }
    };
  }

  storageValue() {
    return async (req, res, next) => {
      try {
        const {
          divideByCategory, category_id, name,
          minValue = 0, maxValue = 1000000000,
        } = req.query;

        let prods;
        let storageValue = [], storageValueByCategory = {};
        const entries = await this._daoEntry.findLatestEntries();

        if (category_id)
          prods = await this._daoProduct.findAllByCategoryComplete(category_id, { order: ['name'] });
        else
          prods = await this._daoProduct.findByQueryComplete({ name }, { order: ['name'] });

        //:: TODO - IMPROVE QUERY to get only last entry of each product dont do it code
        // Get last Entry of Each Product
        const uniqueMask = {};
        for (let e of entries) {
          if (!e.registry || !e.registry.product_id || uniqueMask[e.registry.product_id]) { continue; }
          uniqueMask[e.registry.product_id] = e.get({ plain: true });
        }

        for (let p of prods) {
          let lastProductEntry = uniqueMask[p.product_id];
          if (!lastProductEntry) { continue; }

          let unitPrice = lastProductEntry.price / lastProductEntry.registry.amount;

          // Watching Deploy Error
          if (unitPrice === null || unitPrice === undefined) {
            Logger.critical(`UNIT PRICE IS NULL! > ${lastProductEntry.price} / ${lastProductEntry.registry.amount}`);
            unitPrice = -1;
          }

          const value = {
            product: p,
            valueByUnit: round(unitPrice),
            totalValue: round(unitPrice * p.current_amount),
          };

          //:: Filter by Total Value
          if (value.totalValue < minValue || value.totalValue > maxValue) { continue; }

          // Query By Category
          if (divideByCategory) {
            // `X` - Product has no Category
            let categoriesToAddProduct = p.categories.length ? p.categories : ['X'];

            // Loop for each Product Category
            for (let c of categoriesToAddProduct) {
              let cId = c.category_id ? c.category_id : 'X';

              if (!storageValueByCategory[cId])
                storageValueByCategory[cId] = {
                  categoryName: cId === 'X' ? null : c.name,
                  category: cId === 'X' ? null : c,
                  products: [value],
                  totalValue: value.totalValue
                };
              else {
                storageValueByCategory[cId].products.push(value);
                storageValueByCategory[cId].totalValue += value.totalValue;
              }
            }
          }

          storageValue.push(value);
        }

        // Parse Response
        if (divideByCategory) {
          storageValueByCategory = Object.values(storageValueByCategory);
          storageValueByCategory.sort(sort('categoryName'));
          storageValueByCategory.forEach(c => { c.totalValue = round(c.totalValue); });

          return res.send(storageValueByCategory);
        }

        return res.send(storageValue);
      } catch (err) {
        next(err);
      }
    };
  }
}

module.exports = ControllerReport;
