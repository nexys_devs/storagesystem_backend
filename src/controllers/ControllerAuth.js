const Controller = require('./Controller');
const { DaoUser } = require('./../dao');
const { JWTManager } = require('./../auth');
const { DB_CONSTANTS } = require('./../constants');
const {
  ErrorBadLogin, ErrorInvalidJWT, ErrorNotLoggedIn,
  ErrorRoleNotAuthorized
} = require('./../errors');

class ControllerAuth extends Controller {
  login() {
    return async (req, res, next) => {
      const { login, password } = req.body;

      try {
        let daoUser = new DaoUser();
        let user = await daoUser.authenticate(login, password);
        if (!user) { return new ErrorBadLogin().sendRes(res); }

        let token = await JWTManager.sign({ user_id: user.user_id });

        res.set({ 'Authorization': token });
        res.send(user);
      } catch (err) {
        next(err);
      }
    };
  }

  authenticate() {
    return async (req, res, next) => {
      try {
        let jwt = req.get('Authorization');
        const decoded = await JWTManager.verify(jwt)
          .catch(jwtErr => { throw new ErrorInvalidJWT(jwtErr, req.path); });

        let daoUser = new DaoUser();
        let u = await daoUser.findByPk(decoded.data.user_id);

        if (!u) { return res.sendStatus(401); }
        req.user = u.get({ plain: true });

        next();
      } catch (err) {
        next(err);
      }
    };
  }

  _checkUser() {
    return async (req, _, next) => {
      if (!req || !req.user || !req.user.user_role_id)
        next(new ErrorNotLoggedIn(req.path));
      next();
    };
  }

  _allowRole(role) {
    return [
      this._checkUser(),
      async (req, _, next) => {
        req.user.user_role_id === role ? next() : next(new ErrorRoleNotAuthorized(req.path));
      }
    ];
  }

  _allowRoleAndAbove(role) {
    return [
      this._checkUser(),
      async (req, _, next) => {
        req.user.user_role_id <= role ? next() : next(new ErrorRoleNotAuthorized(req.path));
      }
    ];
  }

  allowOnlyAdmin() {
    return this._allowRole(DB_CONSTANTS.USER_ROLE.ADMIN);
  }

  allowOnlyEditor() {
    return this._allowRole(DB_CONSTANTS.USER_ROLE.EDITOR);
  }

  allowOnlyViewer() {
    return this._allowRole(DB_CONSTANTS.USER_ROLE.VIEWER);
  }

  allowEditorAndAbove() {
    return this._allowRoleAndAbove(DB_CONSTANTS.USER_ROLE.EDITOR);
  }

  allowViewerAndAbove() {
    return this._allowRoleAndAbove(DB_CONSTANTS.USER_ROLE.VIEWER);
  }
}

module.exports = ControllerAuth;