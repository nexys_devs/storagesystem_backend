const _Controller = require('./Controller');
const { Error404 } = require('./../errors');

class ControllerDao extends _Controller {
  constructor(dao, idKey, resourceName) {
    super();
    this.dao = dao;
    this.idKey = idKey;
    this.resourceName = resourceName;
  }

  _UnknownError(path) {
    return new Error404(`Desconhecido ${this.resourceName}`, path);
  }

  _getOffset(req, limit = 25, offset = 0) {
    return { offset: Number(req.query.offset) || offset, limit };
  }

  _getIdKey(req) {
    return req.params[this.idKey];
  }

  getIdKey() {
    return this.idKey;
  }

  findAll() {
    return async (_, res, next) => {
      this.dao.findAll().then(r => res.send(r)).catch(next);
    };
  }

  findByPk() {
    return this._findByPk(id => this.dao.findByPk(id));
  }

  _findByPk(daoCallback) {
    return async (req, res, next) => {
      const ID = this._getIdKey(req);

      try {
        if (!ID) { throw this._UnknownError(req.path); }

        let ret = await daoCallback(ID);
        if (!ret) { throw this._UnknownError(req.path); }

        res.send(ret);
      } catch (err) {
        next(err);
      }
    };
  }

  create() {
    return async (req, res, next) => {
      this.dao.create(req.body).then(p => res.send(p)).catch(next);
    };
  }

  update() {
    return async (req, res, next) => {
      const ID = this._getIdKey(req);

      try {
        if (!ID) { throw this._UnknownError(req.path); }

        await this.dao.updateByPk(ID, req.body);

        res.sendStatus(204);
      } catch (err) {
        next(err);
      }
    };
  }
}

module.exports = ControllerDao;