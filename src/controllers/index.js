const ControllerUD = require('./ControllerUD');
const ControllerUser = require('./ControllerUser');
const ControllerExit = require('./ControllerExit');
const ControllerEntry = require('./ControllerEntry');
const ControllerWorker = require('./ControllerWorker');
const ControllerProduct = require('./ControllerProduct');
const ControllerCategory = require('./ControllerCategory');
const ControllerSupplier = require('./ControllerSupplier');
const ControllerStorage = require('./ControllerStorage');
const ControllerReport = require('./ControllerReport');

const ControllerAuth = new (require('./ControllerAuth'));

module.exports = {
  ControllerAuth,
  CtrlAuth: ControllerAuth,
  ControllerUD: new ControllerUD(),
  ControllerUser: new ControllerUser(),
  ControllerExit: new ControllerExit(),
  ControllerEntry: new ControllerEntry(),
  ControllerReport: new ControllerReport(),
  ControllerWorker: new ControllerWorker(),
  ControllerProduct: new ControllerProduct(),
  ControllerCategory: new ControllerCategory(),
  ControllerSupplier: new ControllerSupplier(),
  ControllerStorage: new ControllerStorage(),
};