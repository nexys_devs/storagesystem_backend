const Joi = require('@hapi/joi');
const { joiNumericID, joiName, joiPhone } = require('./UtilsValidations');

module.exports = {
  create: {
    body: {
      login: Joi.string().trim().min(5).max(30).regex(/^[0-9a-zA-Z_]*$/).required(),
      first_name: joiName().required(),
      last_name: joiName().allow('', null),
      email: Joi.string().trim().max(75).email().required(),
      phone: joiPhone.allow('', null),
      birth: Joi.date(),
      password: Joi.string().min(5).max(20).required(),
      user_role_id: joiNumericID.required()
    }
  },
  update: {
    body: {
      first_name: Joi.string().min(3).max(45).regex(/^[a-zA-Z\s]*$/).required(),
      last_name: Joi.string().min(3).max(45).regex(/^[a-zA-Z\s]*$/).allow('', null),
      phone: joiPhone.allow('', null),
      birth: Joi.date()
    }
  },
  setRole: {
    params: {
      role: Joi.number().integer().min(1).max(3)
    }
  }
};