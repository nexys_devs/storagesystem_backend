const Joi = require('@hapi/joi');
const { joiNumericID, joiOffset } = require('./UtilsValidations');

module.exports = {
  findAll: {
    offset: joiOffset
  },
  create: {
    body: {
      name: Joi.string().trim().min(3).max(45).required(),
      ud_id: joiNumericID.required(),
      warning_amount: Joi.number().positive().allow(0),
      danger_amount: Joi.number().positive().allow(0),
      description: Joi.string().trim().max(200).allow('', null),
      categories: Joi.array().items(joiNumericID)
    }
  },
  update: {
    body: {
      name: Joi.string().trim().min(3).max(45),
      ud_id: joiNumericID,
      warning_amount: Joi.number().positive().allow(0),
      danger_amount: Joi.number().positive().allow(0),
      description: Joi.string().trim().max(200).allow('', null),
      categories: Joi.array().items(joiNumericID)
    }
  },
  productCategory: {
    body: Joi.array().items(joiNumericID),
    options: {
      contextRequest: true,
    }
  },
  query: {
    query: Joi.object({
      name: Joi.string(),
    }).required()
  }
};