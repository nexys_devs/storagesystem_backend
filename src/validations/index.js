module.exports = {
  ValidationUD: require('./ValidationUD'),
  ValidationUser: require('./ValidationUser'),
  ValidationExit: require('./ValidationExit'),
  ValidationLogin: require('./ValidationLogin'),
  ValidationEntry: require('./ValidationEntry'),
  ValidationWorker: require('./ValidationWorker'),
  ValidationReport: require('./ValidationReport'),
  ValidationProduct: require('./ValidationProduct'),
  ValidationCategory: require('./ValidationCategory'),
  ValidationSupplier: require('./ValidationSupplier'),
  ValidationStorage: require('./ValidationStorage'),
};