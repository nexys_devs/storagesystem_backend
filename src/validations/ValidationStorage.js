const Joi = require('@hapi/joi');
const { joiName, joiOffset } = require('./UtilsValidations');

module.exports = {
  findAll: {
    offset: joiOffset
  },
  create: {
    body: {
      name: joiName().required(),
      description: Joi.string().trim().max(200).allow('', null)
    }
  },
  update: {
    body: {
      name: joiName(),
      description: Joi.string().trim().max(200).allow('', null)
    }
  },
  query: {
    query: Joi.object({
      name: Joi.string(),
    }).required()
  }
};