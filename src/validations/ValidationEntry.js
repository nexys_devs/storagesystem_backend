const Joi = require('@hapi/joi');
const { joiNumericID } = require('./UtilsValidations');

module.exports = {
  create: {
    body: {
      amount: Joi.number().positive().required(),
      product_id: joiNumericID.required(),
      price: Joi.number().positive().required(),
      supplier_id: joiNumericID.required()
    }
  },
  update: {
    body: {
      amount: Joi.number().positive().required(),
      product_id: joiNumericID.required(),
      price: Joi.number().positive(),
      supplier_id: joiNumericID
    }
  },
  filter: {
    query: {
      priceMin: Joi.number().positive().allow(0),
      priceMax: Joi.number().positive().allow(0),
      amountMin: Joi.number().positive().allow(0),
      amountMax: Joi.number().positive().allow(0),
      supplierId: joiNumericID,
      productId: joiNumericID,
      productName: Joi.string().trim(),
      timeMin: Joi.string().trim(),
      timeMax: Joi.string().trim(),
    }
  }
};