const Joi = require('@hapi/joi');
const { joiName, joiPhone, joiOffset } = require('./UtilsValidations');

module.exports = {
  findAll: {
    offset: joiOffset
  },
  create: {
    body: {
      name: joiName().required(),
      company: joiName().required(),
      email: Joi.string().trim().email(),
      phone: joiPhone.allow('', null),
      address: Joi.string().max(200).allow('', null),
      description: Joi.string().max(200).allow('', null),
    }
  },
  update: {
    body: {
      name: joiName(),
      company: joiName(),
      email: Joi.string().trim().email(),
      phone: joiPhone.trim().allow('', null),
      address: Joi.string().trim().max(200).allow('', null),
      description: Joi.string().trim().max(200).allow('', null),
    }
  },
  query: {
    query: Joi.object({
      name: Joi.string(),
    }).required()
  }
};