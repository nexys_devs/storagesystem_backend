const Joi = require('@hapi/joi');
const { joiNumericID } = require('./UtilsValidations');

module.exports = {
  create: {
    body: {
      amount: Joi.number().positive().required(),
      product_id: joiNumericID.required(),
      worker_id: joiNumericID.required(),
      storage_id: joiNumericID.required()
    }
  },
  update: {
    body: {
      amount: Joi.number().positive().required(),
      product_id: joiNumericID.required(),
      worker_id: joiNumericID,
      storage_id: joiNumericID
    }
  },
  filter: {
    query: {
      amountMin: Joi.number().positive().allow(0),
      amountMax: Joi.number().positive().allow(0),
      timeMin: Joi.string().trim(),
      timeMax: Joi.string().trim(),
      productId: joiNumericID,
      productName: Joi.string().trim(),
      storageId: joiNumericID,
      storageName: Joi.string().trim(),
      workerId: joiNumericID,
      workerName: Joi.string().trim(),
    }
  }
};