const Joi = require('@hapi/joi');
const { joiName, joiOffset } = require('./UtilsValidations');

module.exports = {
  findAll: {
    offset: joiOffset
  },
  create: {
    body: {
      name: joiName(2).required(),
    }
  },
  update: {
    body: {
      name: joiName(2).required(),
    }
  },
  query: {
    query: Joi.object({
      name: Joi.string(),
    }).required()
  }
};