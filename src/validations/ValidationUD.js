const Joi = require('@hapi/joi');

module.exports = {
  create: {
    body: {
      code: Joi.string().min(1).max(5).required(),
      description: Joi.string().trim().max(200).allow('', null)
    }
  },
  update: {
    body: {
      code: Joi.string().min(1).max(5),
      description: Joi.string().trim().max(200).allow('', null)
    }
  },
  query: {
    query: Joi.object({
      code: Joi.string(),
    }).required()
  }
};