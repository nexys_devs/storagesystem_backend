const Joi = require('@hapi/joi');

module.exports = {
  joiNumericID: Joi.number().positive().integer(),
  joiName: (min = 3, max = 45) => {
    return  Joi.string().trim().min(min).max(max).regex(/^[A-zÀ-ú\s]*$/);
  },
  joiPhone: Joi.string().trim().max(15).regex(/^[0-9]*$/),
  joiOffset: Joi.number().integer().positive().default(0)
};