const Joi = require('@hapi/joi');
const { joiNumericID } = require('./UtilsValidations');

module.exports = {
  shoplist: {
    query: {
      level: Joi.string().trim().default('')
    }
  },
  storageValue: {
    query: {
      divideByCategory: Joi.string().valid('true'),
      minValue: Joi.number().positive().default(0),
      maxValue: Joi.number().positive().default(100000000),
      name: Joi.string().trim().min(3).max(45),
      category_id: joiNumericID
    }
  }
};