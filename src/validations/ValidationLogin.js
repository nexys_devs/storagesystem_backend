const Joi = require('@hapi/joi');

module.exports = {
  body: {
    login: Joi.string().required().trim(),
    password: Joi.string().required()
  }
};