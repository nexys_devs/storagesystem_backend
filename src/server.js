require('dotenv').config();

const express = require('express');
const Models = require('./models');
const { Logger } = require('./utils');
const { MainApp } = require('./apps');

const { PORT } = process.env;

const server = express();
server.on('error', err => Logger.critical(`[SERVER][ERROR][${err}]`));

server.use(MainApp);

Models.sequelize.authenticate().then(() => {
  Logger.warn('[BOOT][CONNECT DATABASE][SUCCESS]');

  server.listen(PORT, () => {
    Logger.warn(`[BOOT][SERVER LISTEN][SUCCESS][PORT ${PORT}]`);
  });
}).catch(err => {
  console.error(err);
  Logger.error('[BOOT][CONNECT DATABASE][ERROR]', err);
});