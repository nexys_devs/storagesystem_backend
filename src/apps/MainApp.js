require('dotenv').config();
const express = require('express');
const helmet = require('helmet');
const MorganFreeman = require('morgan');

const { Logger } = require('./../utils');
const { Errorhandler } = require('./../errors');

const { ControllerAuth } = require('./../controllers');
const {
  RouterUser, RouterAuth, RouteProduct, RouteCategory,
  RouterUD, RouteSupplier, RouteStorage, RouteWorker,
  RouteEntry, RouteExit, RouteReport
} = require('./../routers');

const app = express();

app.use(helmet());
app.use(express.json());
app.use(MorganFreeman('combined', { stream: Logger.morganStream('request') }));

//:: Ping
app.get('/ping', async (_, res) => {
  res.write('Pong!'); res.end();
});

//:: Auth
app.use('/auth', RouterAuth);
app.use(ControllerAuth.authenticate());

//:: Routes
app.use('/user', RouterUser);
app.use('/product', RouteProduct);
app.use('/category', RouteCategory);
app.use('/ud', RouterUD);
app.use('/supplier', RouteSupplier);
app.use('/storage', RouteStorage);
app.use('/worker', RouteWorker);
app.use('/entry', RouteEntry);
app.use('/exit', RouteExit);
app.use('/report', RouteReport);

app.use(Errorhandler);
app.use(async (_, res) => res.status(404).send('Unknown route'));

module.exports = app;