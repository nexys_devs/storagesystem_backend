const { Router } = require('express');
const Validate = require('express-validation');
const { ValidationUser } = require('../validations');
const { ControllerUser, CtrlAuth } = require('../controllers');

const router = Router();

router.use(CtrlAuth.allowOnlyAdmin());
router.get('/', ControllerUser.findAll());
router.get('/:userId(\\d+)', ControllerUser.findByPk());

router.post('/', Validate(ValidationUser.create), ControllerUser.create());
router.put('/:userId(\\d+)', Validate(ValidationUser.update), ControllerUser.update());

router.put('/:userId(\\d+)/setRole/:role(\\d+)', Validate(ValidationUser.setRole), ControllerUser.setRole());

module.exports = router;