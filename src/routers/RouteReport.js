const { Router } = require('express');
const Validate = require('express-validation');
const { ControllerReport } = require('../controllers');
const { ValidationReport } = require('../validations');

const router = Router();

router.get('/shoplist', Validate(ValidationReport.shoplist), ControllerReport.shoplist());

router.get('/storageValue', Validate(ValidationReport.storageValue), ControllerReport.storageValue());

module.exports = router;