const { Router } = require('express');
const Validate = require('express-validation');
const { ValidationEntry } = require('../validations');
const { ControllerEntry, CtrlAuth } = require('./../controllers');

const router = Router();

router.use(CtrlAuth.allowViewerAndAbove());
router.get('/', ControllerEntry.findAllIncludeProduct());
router.get('/:entryId(\\d+)', ControllerEntry.findByPkIncludeRegistry());
router.get('/:entryId(\\d+)/include/product', ControllerEntry.findByPkIncludeProduct());

// Filters
router.get('/filter', Validate(ValidationEntry.filter), ControllerEntry.query());
router.get('/by/product/:productId(\\d+)', ControllerEntry.findAllByProduct());
router.get('/by/user/:userId(\\d+)', ControllerEntry.findAllByUser());
router.get('/by/supplier/:supplierId(\\d+)', ControllerEntry.findAllBySupplier());

router.use(CtrlAuth.allowEditorAndAbove());
router.post('/', Validate(ValidationEntry.create), ControllerEntry.create());
router.put('/:entryId(\\d+)', Validate(ValidationEntry.update), ControllerEntry.update());

module.exports = router;