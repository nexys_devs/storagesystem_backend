const { Router } = require('express');
const Validate = require('express-validation');
const { ValidationSupplier } = require('../validations');
const { ControllerSupplier, CtrlAuth } = require('./../controllers');

const router = Router();

router.use(CtrlAuth.allowViewerAndAbove());
router.get('/', Validate(ValidationSupplier.findAll), ControllerSupplier.findAll());
router.get('/all/simple', ControllerSupplier.findAllSimple());
router.get('/:supplierId(\\d+)', ControllerSupplier.findByPk());
router.get('/query', Validate(ValidationSupplier.query), ControllerSupplier.findByQuery());

router.use(CtrlAuth.allowEditorAndAbove());
router.post('/', Validate(ValidationSupplier.create), ControllerSupplier.create());
router.put('/:supplierId(\\d+)', Validate(ValidationSupplier.update), ControllerSupplier.update());

module.exports = router;