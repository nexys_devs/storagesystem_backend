const { Router } = require('express');
const Validate = require('express-validation');
const { ValidationLogin } = require('../validations');
const { ControllerAuth } = require('./../controllers');

const router = Router();

router.post('/login', Validate(ValidationLogin), ControllerAuth.login());

module.exports = router;