const { Router } = require('express');
const Validate = require('express-validation');
const { ValidationCategory } = require('../validations');
const { ControllerCategory, CtrlAuth } = require('../controllers');

const router = Router();

router.use(CtrlAuth.allowViewerAndAbove());
router.get('/', Validate(ValidationCategory.findAll), ControllerCategory.findAll());
router.get('/all/simple', ControllerCategory.findAllSimple());
router.get('/:categoryId(\\d+)', ControllerCategory.findByPk());
router.get('/query', Validate(ValidationCategory.query), ControllerCategory.findByQuery());

router.use(CtrlAuth.allowEditorAndAbove());
router.post('/', Validate(ValidationCategory.create), ControllerCategory.create());
router.put('/:categoryId(\\d+)', Validate(ValidationCategory.update), ControllerCategory.update());

module.exports = router;