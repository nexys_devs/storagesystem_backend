const { Router } = require('express');
const Validate = require('express-validation');
const { ValidationUD } = require('../validations');
const { ControllerUD, CtrlAuth } = require('../controllers');

const router = Router();

router.use(CtrlAuth.allowViewerAndAbove());
router.get('/', ControllerUD.findAll());
router.get('/all/simple', ControllerUD.findAllSimple());
router.get('/:udId(\\d+)', ControllerUD.findByPk());
router.get('/query', Validate(ValidationUD.query), ControllerUD.findByQuery());

router.use(CtrlAuth.allowEditorAndAbove());
router.post('/', Validate(ValidationUD.create), ControllerUD.create());
router.put('/:udId(\\d+)', Validate(ValidationUD.update), ControllerUD.update());

module.exports = router;