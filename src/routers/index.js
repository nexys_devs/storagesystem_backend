module.exports = {
  RouterUD: require('./RouterUD'),
  RouterAuth: require('./RouterAuth'),
  RouteExit: require('./RouteExit'),
  RouteEntry: require('./RouteEntry'),
  RouterUser: require('./RouterUser'),
  RouteWorker: require('./RouteWorker'),
  RouteReport: require('./RouteReport'),
  RouteProduct: require('./RouteProduct'),
  RouteCategory: require('./RouteCategory'),
  RouteSupplier: require('./RouteSupplier'),
  RouteStorage: require('./RouteStorage')
};