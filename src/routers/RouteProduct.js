const { Router } = require('express');
const Validate = require('express-validation');
const { ValidationProduct } = require('../validations');
const { ControllerProduct, CtrlAuth } = require('./../controllers');

const router = Router();

router.use(CtrlAuth.allowViewerAndAbove());
router.get('/', Validate(ValidationProduct.findAll), ControllerProduct.findAllComplete());
router.get('/all/simple', ControllerProduct.findAllSimple());
router.get('/:productId(\\d+)', ControllerProduct.findByPk());
router.get('/query', Validate(ValidationProduct.query), ControllerProduct.findByQuery());

router.use(CtrlAuth.allowEditorAndAbove());

router.post('/', Validate(ValidationProduct.create), ControllerProduct.create());
router.put('/:productId(\\d+)', Validate(ValidationProduct.update), ControllerProduct.update());

router.post('/:productId(\\d+)/category/add',
  Validate(ValidationProduct.productCategory), ControllerProduct.addProductCategory());

router.post('/:productId(\\d+)/category/remove',
  Validate(ValidationProduct.productCategory), ControllerProduct.removeProductCategory());

router.post('/:productId(\\d+)/category/set',
  Validate(ValidationProduct.productCategory), ControllerProduct.setProductCategory());

router.use(CtrlAuth.allowOnlyAdmin());
router.delete('/:productId(\\d+)', ControllerProduct.delete());

module.exports = router;