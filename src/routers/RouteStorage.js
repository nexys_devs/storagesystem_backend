const { Router } = require('express');
const Validate = require('express-validation');
const { ValidationStorage } = require('../validations');
const { ControllerStorage, CtrlAuth } = require('../controllers');

const router = Router();

router.use(CtrlAuth.allowViewerAndAbove());
router.get('/', Validate(ValidationStorage.findAll), ControllerStorage.findAll());
router.get('/all/simple', ControllerStorage.findAllSimple());
router.get('/:storageId(\\d+)', ControllerStorage.findByPk());
router.get('/query', Validate(ValidationStorage.query), ControllerStorage.findByQuery());

router.use(CtrlAuth.allowEditorAndAbove());
router.post('/', Validate(ValidationStorage.create), ControllerStorage.create());
router.put('/:storageId(\\d+)', Validate(ValidationStorage.update), ControllerStorage.update());

module.exports = router;