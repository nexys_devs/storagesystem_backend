const { Router } = require('express');
const Validate = require('express-validation');
const { ValidationExit } = require('../validations');
const { ControllerExit, CtrlAuth } = require('./../controllers');

const router = Router();

router.use(CtrlAuth.allowViewerAndAbove());
router.get('/', ControllerExit.findAllIncludeProduct());
router.get(('/:exitId(\\d+)'), ControllerExit.findByPkIncludeRegistry());
router.get('/:exitId(\\d+)/include/product', ControllerExit.findByPkIncludeProduct());

// Filters
router.get('/filter', Validate(ValidationExit.filter), ControllerExit.query());
router.get('/by/product/:productId(\\d+)', ControllerExit.findAllByProduct());
router.get('/by/user/:userId(\\d+)', ControllerExit.findAllByUser());
router.get('/by/worker/:workerId(\\d+)', ControllerExit.findAllByWorker());
router.get('/by/storage/:storageId(\\d+)', ControllerExit.findAllByStorage());

router.use(CtrlAuth.allowEditorAndAbove());
router.post('/', Validate(ValidationExit.create), ControllerExit.create());
router.put(('/:exitId(\\d+)'), Validate(ValidationExit.update), ControllerExit.update());

module.exports = router;