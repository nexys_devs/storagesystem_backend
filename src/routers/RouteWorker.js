const { Router } = require('express');
const Validate = require('express-validation');
const { ValidationWorker } = require('../validations');
const { ControllerWorker, CtrlAuth } = require('./../controllers');

const router = Router();

router.use(CtrlAuth.allowViewerAndAbove());
router.get('/', Validate(ValidationWorker.findAll), ControllerWorker.findAll());
router.get('/all/simple', ControllerWorker.findAllSimple());
router.get('/:workerId(\\d+)', ControllerWorker.findByPk());
router.get('/query', Validate(ValidationWorker.query), ControllerWorker.findByQuery());

router.use(CtrlAuth.allowEditorAndAbove());
router.post('/', Validate(ValidationWorker.create), ControllerWorker.create());
router.put('/:workerId(\\d+)', Validate(ValidationWorker.update), ControllerWorker.update());

module.exports = router;