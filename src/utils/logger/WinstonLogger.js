const Winston = require('winston');

const { NODE_ENV } = process.env;
const _NODE_ENV = String(NODE_ENV).toUpperCase();

let i = 0;
const LOG_LEVELS = {
  critical: i++,
  error: i++,
  warn: i++,
  info: i++,
  http: i++,
  verbose: i++,
  debug: i++,
  silly: i++
};

const _createLogger = (level, fileName, extra = {}) => (
  new Winston.createLogger({
    levels: LOG_LEVELS,
    format: Winston.format.combine(
      Winston.format.timestamp(),
      Winston.format.json()
    ),
    transports: [
      new Winston.transports.File({
        filename: `logs/${fileName ? fileName : level}.log`,
        level: level,
      })
    ],
    ...extra
  })
);

const createMorganLogger = name => {
  let morg = _createLogger('info', name);
  morg.stream = {
    write: function (message) {
      morg.info(message);
    }
  };

  return morg;
};

module.exports = {
  error: _createLogger('error', 'errors', {
    exceptionHandlers: _NODE_ENV === 'DEV' ? Winston.transports.Console.log : [
      new Winston.transports.File({ filename: 'logs/exceptions.log' })
    ]
  }),
  critical: _createLogger('critical'),
  warn: _createLogger('warn'),
  info: _createLogger('info'),
  debug: _createLogger('debug'),

  createMorganLogger
};