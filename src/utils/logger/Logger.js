const WinstonLogger = require('./WinstonLogger');
const COLORS = require('./LoggerColors');

const { NODE_ENV } = process.env;
const _NODE_ENV = String(NODE_ENV).toUpperCase();

function devLogger (color, ...str) {
  str.forEach(s => console.log(`${color}%s${COLORS.Reset}`, s));
}

const _log = _NODE_ENV === 'DEV' ? devLogger : () => {};

module.exports = {
  log: function (...str) {
    _log(COLORS.FgWhite, ...str);
  },

  critical: function (...str) {
    _log(COLORS.Critical, ...str);
    WinstonLogger.critical.error(str);
  },

  error: function (msg, err) {
    _log(COLORS.FgRed, msg, err);
    WinstonLogger.error.error({ message: msg, error: err });
  },

  warn: function (...str) {
    _log(COLORS.FgYellow, ...str);
    WinstonLogger.warn.warn(...str);
  },

  info: function (...str) {
    _log(COLORS.FgGreen, ...str);
    WinstonLogger.info.info(...str);
  },

  debug: function (...str) {
    _log(COLORS.FgMagenta, ...str);
  },

  morganStream: file => WinstonLogger.createMorganLogger(file).stream
};