module.exports = {
  Logger: require('./logger/Logger'),
  round: (n, x = 100) => Math.round(n * x) / x,
  sort: prop => (a, b) => a[prop] < b[prop] ? -1 : (a[prop] > b[prop] ? 1 : 0)
};