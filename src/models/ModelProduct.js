const { Model } = require('sequelize');

class ModelProduct extends Model {
  static init(sequelize, Sequelize) {
    return super.init({
      product_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.STRING(45),
        unique: true,
        allowNull: false
      },
      warning_amount: {
        type: Sequelize.FLOAT,
        defaultValue: 0
      },
      danger_amount: {
        type: Sequelize.FLOAT,
        defaultValue: 0
      },
      current_amount: {
        type: Sequelize.FLOAT,
        defaultValue: 0
      },
      description: {
        type: Sequelize.STRING(200),
        defaultValue: ''
      }
    }, {
      sequelize,
      modelName: 'Product',
      tableName: 'product',
      timestamps: false,
    });
  }

  static associate(models) {
    models.Product.belongsToMany(models.Category, {
      through: 'ProductCategory',
      foreignKey: { name: 'product_id', allowNull: false },
      as: 'categories',
      onDelete: 'cascade'
    });

    models.Product.belongsTo(models.UD, {
      foreignKey: {
        name: 'ud_id',
        allowNull: false
      },
      as: 'ud',
    });

    models.Product.hasMany(models.Registry, {
      foreignKey: { name: 'product_id', allowNull: false },
      as: 'registries'
    });
  }
}

module.exports = ModelProduct;