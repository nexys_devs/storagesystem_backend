const { Model } = require('sequelize');

class ModelUser extends Model {
  static init(sequelize, Sequelize) {
    return super.init({
      user_auth_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      secret: {
        type: Sequelize.STRING(300),
        allowNull: false,
      }
    }, {
      sequelize,
      modelName: 'UserAuth',
      tableName: 'user_auth',
      timestamps: false
    });
  }

  static associate(models) {
    models.UserAuth.belongsTo(models.User, {
      foreignKey: { name: 'user_id', allowNull: false },
      as: 'user'
    });
  }
}

module.exports = ModelUser;