const { Model } = require('sequelize');

class ModelExit extends Model {
  static init(sequelize, Sequelize) {
    return super.init({
      exit_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      }
    }, {
      sequelize,
      modelName: 'Exit',
      tableName: 'exit',
      timestamps: false
    });
  }

  static associate(models) {
    models.Exit.belongsTo(models.Registry, {
      foreignKey: { name: 'exit_id', allowNull: false },
      as: 'registry',
      targetKey: 'registry_id',
      onDelete: 'cascade'
    });

    models.Exit.belongsTo(models.Worker, {
      foreignKey: { name: 'worker_id', allowNull: false },
      as: 'worker'
    });

    models.Exit.belongsTo(models.Storage, {
      foreignKey: { name: 'storage_id', allowNull: false },
      as: 'storage'
    });
  }
}

module.exports = ModelExit;