const { Model } = require('sequelize');

class ModelUser extends Model {
  static init(sequelize, Sequelize) {
    return super.init({
      user_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      login: {
        type: Sequelize.STRING(30),
        unique: true,
        allowNull: false
      },
      first_name: {
        type: Sequelize.STRING(45),
        allowNull: false
      },
      last_name: {
        type: Sequelize.STRING(45),
        defaultValue: ''
      },
      email: {
        type: Sequelize.STRING(75),
        unique: true,
        allowNull: false
      },
      phone: {
        type: Sequelize.STRING(15),
        defaultValue: ''
      },
      birth: {
        type: Sequelize.DATEONLY,
        //allowNull: false
      }
    }, {
      sequelize,
      modelName: 'User',
      tableName: 'user',
      timestamps: false
    });
  }

  static associate(models) {
    models.User.belongsTo(models.UserRole, {
      foreignKey: { name: 'user_role_id', allowNull: false },
      as: 'role'
    });

    models.User.hasOne(models.UserAuth, {
      foreignKey: { name: 'user_id', allowNull: false },
      as: 'auth'
    });

    models.User.hasMany(models.Registry, {
      foreignKey: { name: 'user_id', allowNull: false },
      as: 'registries'
    });
  }
}

module.exports = ModelUser;