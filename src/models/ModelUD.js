const { Model } = require('sequelize');

class ModelUD extends Model {
  static init(sequelize, Sequelize) {
    return super.init({
      ud_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      code: {
        type: Sequelize.STRING(5),
        unique: true,
        allowNull: false
      },
      description: {
        type: Sequelize.STRING(200),
        defaultValue: ''
      }
    }, {
      sequelize,
      modelName: 'UD',
      tableName: 'ud',
      timestamps: false
    });
  }

  static associate(models) {
    models.UD.hasMany(models.Product, {
      foreignKey: { name: 'ud_id', allowNull: false },
      as: 'products'
    });
  }
}

module.exports = ModelUD;