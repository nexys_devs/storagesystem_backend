const { Model } = require('sequelize');

class ModelCategory extends Model {
  static init(sequelize, Sequelize) {
    return super.init({
      category_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.STRING(45),
        unique: true,
        allowNull: false
      },
      description: {
        type: Sequelize.STRING(200),
        defaultValue: ''
      }
    }, {
      sequelize,
      modelName: 'Category',
      tableName: 'category',
      timestamps: false
    });
  }

  static associate(models) {
    models.Category.belongsToMany(models.Product, {
      through: 'ProductCategory',
      foreignKey: { name: 'category_id', allowNull: false },
      as: 'products',
      onDelete: 'cascade'
    });
  }
}

module.exports = ModelCategory;