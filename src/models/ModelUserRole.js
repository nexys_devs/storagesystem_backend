const { Model } = require('sequelize');

class ModelUserRole extends Model {
  static init(sequelize, Sequelize) {
    return super.init({
      user_role_id: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      name: {
        type: Sequelize.STRING(20),
        allowNull: false
      },
      description: {
        type: Sequelize.STRING(200),
        defaultValue: ''
      }
    }, {
      sequelize,
      modelName: 'UserRole',
      tableName: 'user_role',
      timestamps: false
    });
  }

  static associate(models) {
    models.UserRole.hasMany(models.User, {
      foreignKey: { name: 'user_role_id', allowNull: false },
      as: 'users'
    });
  }
}

module.exports = ModelUserRole;