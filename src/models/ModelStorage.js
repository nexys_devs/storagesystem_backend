const { Model } = require('sequelize');

class ModelStorage extends Model {
  static init(sequelize, Sequelize) {
    return super.init({
      storage_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.STRING(45),
        unique: true,
        allowNull: false
      },
      description: {
        type: Sequelize.STRING(200),
        defaultValue: ''
      }
    }, {
      sequelize,
      modelName: 'Storage',
      tableName: 'storage',
      timestamps: false
    });
  }

  static associate(models) {
    models.Storage.hasMany(models.Exit, {
      foreignKey: { name: 'storage_id', allowNull: false },
      as: 'exits'
    });
  }
}

module.exports = ModelStorage;