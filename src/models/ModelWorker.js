const { Model } = require('sequelize');

class ModelWorker extends Model {
  static init(sequelize, Sequelize) {
    return super.init({
      worker_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.STRING(45),
        allowNull: false,
      }
    }, {
      sequelize,
      modelName: 'Worker',
      tableName: 'worker',
      timestamps: false
    });
  }

  static associate(models) {
    models.Worker.hasMany(models.Exit, {
      foreignKey: { name: 'worker_id', allowNull: false },
      as: 'exits'
    });
  }
}

module.exports = ModelWorker;