const { Model } = require('sequelize');

class ModelProductCategory extends Model {
  static init(sequelize, Sequelize) {
    return super.init({
      product_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        references: {
          model: 'Product',
          key: 'product_id'
        }
      },
      category_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        references: {
          model: 'Category',
          key: 'category_id'
        }
      }
    }, {
      sequelize,
      modelName: 'ProductCategory',
      tableName: 'product_category',
      timestamps: false
    });
  }

  static associate(_models) { }
}

module.exports = ModelProductCategory;