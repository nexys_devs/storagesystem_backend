const Sequelize = require('sequelize');
const { SequelizeErrorHandler } = require('./../errors');

const ModelUser = require('./ModelUser');
const ModelUserAuth = require('./ModelUserAuth');
const ModelUserRole = require('./ModelUserRole');

const ModelProduct = require('./ModelProduct');
const ModelCategory = require('./ModelCategory');
const ModelProductCategory = require('./ModelProductCategory');
const ModelUD = require('./ModelUD');

const ModelRegistry = require('./ModelRegistry');
const ModelEntry = require('./ModelEntry');
const ModelSupplier = require('./ModelSupplier');
const ModelExit = require('./ModelExit');
const ModelWorker = require('./ModelWorker');
const ModelStorage = require('./ModelStorage');

const sequelize = new Sequelize(process.env.DATABASE_URL, {
  dialect: process.env.DB_DIALECT,
  pool: {
    max: 10,
    min: 0,
    idle: 10000
  },
  logging: process.env.DB_LOG === 'TRUE' ? console.log : false,
});

//:: Attach Error Handler
sequelize.query = function () {
  return Sequelize.prototype.query.apply(this, arguments)
    .catch(err => {
      console.log(err);
      throw SequelizeErrorHandler(err, sequelize, Sequelize);
    });
};

const Models = {
  Sequelize: Sequelize,
  sequelize: sequelize,

  User: ModelUser.init(sequelize, Sequelize),
  UserAuth: ModelUserAuth.init(sequelize, Sequelize),
  UserRole: ModelUserRole.init(sequelize, Sequelize),

  Product: ModelProduct.init(sequelize, Sequelize),
  UD: ModelUD.init(sequelize, Sequelize),
  Category: ModelCategory.init(sequelize, Sequelize),
  ProductCategory: ModelProductCategory.init(sequelize, Sequelize),

  Registry: ModelRegistry.init(sequelize, Sequelize),
  Entry: ModelEntry.init(sequelize, Sequelize),
  Supplier: ModelSupplier.init(sequelize, Sequelize),
  Exit: ModelExit.init(sequelize, Sequelize),
  Worker: ModelWorker.init(sequelize, Sequelize),
  Storage: ModelStorage.init(sequelize, Sequelize)
};

Object.keys(Models).forEach(key => {
  if (Models[key]['associate'])
    Models[key].associate(Models);
});

module.exports = Models;