const { Model } = require('sequelize');

class ModelRegistry extends Model {
  static init(sequelize, Sequelize) {
    return super.init({
      registry_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      amount: {
        type: Sequelize.FLOAT,
        defaultValue: 0
      },
      timestamp: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      }
    }, {
      sequelize,
      modelName: 'Registry',
      tableName: 'registry',
      timestamps: false
    });
  }

  static associate(models) {
    models.Registry.belongsTo(models.Product, {
      foreignKey: { name: 'product_id', allowNull: false },
      as: 'product',
      onDelete: 'cascade'
    });

    models.Registry.belongsTo(models.User, {
      foreignKey: { name: 'user_id', allowNull: false },
      as: 'user'
    });
  }
}

module.exports = ModelRegistry;