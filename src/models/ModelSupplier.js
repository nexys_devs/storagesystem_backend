const { Model } = require('sequelize');

class ModelSupplier extends Model {
  static init(sequelize, Sequelize) {
    return super.init({
      supplier_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.STRING(45),
        unique: true,
        allowNull: false
      },
      company: {
        type: Sequelize.STRING(45),
        defaultValue: ''
      },
      email: {
        type: Sequelize.STRING(75),
        defaultValue: ''
      },
      phone: {
        type: Sequelize.STRING(15),
        defaultValue: ''
      },
      address: {
        type: Sequelize.STRING(200),
        defaultValue: ''
      },
      description: {
        type: Sequelize.STRING(200),
        defaultValue: ''
      }
    }, {
      sequelize,
      modelName: 'Supplier',
      tableName: 'supplier',
      timestamps: false
    });
  }

  static associate(models) {
    models.Supplier.hasMany(models.Entry, {
      foreignKey: { name: 'supplier_id', allowNull: false },
      as: 'entries'
    });
  }
}

module.exports = ModelSupplier;