const { Model } = require('sequelize');

class ModelEntry extends Model {
  static init(sequelize, Sequelize) {
    return super.init({
      entry_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      price: {
        type: Sequelize.FLOAT,
        defaultValue: 0
      }
    }, {
      sequelize,
      modelName: 'Entry',
      tableName: 'entry',
      timestamps: false
    });
  }

  static associate(models) {
    models.Entry.belongsTo(models.Registry, {
      foreignKey: { name: 'entry_id', allowNull: false },
      as: 'registry',
      targetKey: 'registry_id',
      onDelete: 'cascade'
    });

    models.Entry.belongsTo(models.Supplier, {
      foreignKey: { name: 'supplier_id', allowNull: false },
      as: 'supplier'
    });
  }
}

module.exports = ModelEntry;