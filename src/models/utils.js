const fs = require('fs');

module.exports.runSqlScript = function (Models, path) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, (err, sql) => {
      if (err) { return reject(err); }

      var promises = [];
      var lines = sql.toString().split('\n');

      for (var line of lines) {
        if (line.trim() === '') { continue; }
        if (line.indexOf('--') == 0) { continue; }

        for (let statement of line.split(';')) {
          if (statement.trim() === '') { continue; }

          if (statement[statement.length - 1] !== ';')
            statement += ';';

          promises.push(Models.sequelize.query(statement));
        }
      }

      Promise.all(promises).then(_ => resolve()).catch(err => reject(err));
    });
  });
};