
INSERT INTO public."user" (login, first_name, email, user_role) VALUES ('Teste', 'Teste', 'Teste@test.com', 1);

-- Password > %password%
INSERT INTO public.user_auth(user_auth_id, secret, user_id) VALUES (1, '$2a$10$VzS9CikMsausJVZA3ihF/ul3LkJCCAgBMiGrR9P0R4jV2v4KuylUC', 1);

INSERT INTO product (name, ud_id) VALUES ('Batata', 1);
INSERT INTO storage (name) VALUES ('Storage');
INSERT INTO worker (name) VALUES ('Worker');

INSERT INTO supplier (name) values ('Supplier');

INSERT INTO category (name) VALUES ('Category A');
INSERT INTO category (name) VALUES ('Category B');
