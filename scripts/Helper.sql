
-- Select all Registries by Product
SELECT * from registry as r
inner join product as p on p.product_id = r.product_id
where p.product_id = 395;

-- Select all Entries By Product
select * from entry as e
inner join registry as r on r.registry_id = entry_id
where e.entry_id = 395;

-- Simple
select * from registry where registry_id = 395
select * from entry where entry_id = 395
select * from product where product_id = 328

-- Alter Constraint
ALTER TABLE "registry"
DROP CONSTRAINT "registry_product_id_fkey",
ADD CONSTRAINT "registry_product_id_fkey" FOREIGN KEY ("product_id")
	REFERENCES "product" (product_id)
	ON DELETE CASCADE

select *
from information_schema.key_column_usage
where position_in_unique_constraint is not null