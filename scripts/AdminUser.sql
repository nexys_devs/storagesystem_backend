INSERT INTO public."user"(user_id, login, first_name, last_name, email, user_role_id) VALUES (1, 'Admin', 'Admin', 'Admin', 'admin@boss.com', 1);

-- '%secret_pasword%'
INSERT INTO user_auth (user_id, secret) VALUES (1, '$2a$10$UvwUPSdhnLl22jriwTs0zuSVRF.8lLxcJP9aA/II3wcgouYZpKDky');
