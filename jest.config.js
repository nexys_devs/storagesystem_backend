module.exports = {
  //automock: true,
  testEnvironment: "<rootDir>/__tests__/setup/TestEnviroment.js",
  coveragePathIgnorePatterns: [
    "/node_modules/"
  ],
  testPathIgnorePatterns: [
    "/_helper/*",
    "/setup/*",
    "/simulations/*",
    "/test.js",
    "/src/*"
  ],
  globalSetup: "<rootDir>/__tests__/setup/setup-globals.js",
};