const request = require('supertest');
const { MainApp } = require('../../../src/apps');
const DummyFactory = require('../../setup/DummyFactory');

const {
  DUMMY_UD, DUMMY_STORAGE,
  DUMMY_WORKER, DUMMY_SUPPLIER, DUMMY_ENTRY
} = require('../../setup/Dummy');

let TOK_EDITOR;

beforeAll(async done => {
  await DummyFactory.createUd(DUMMY_UD);
  await DummyFactory.createWorker(DUMMY_WORKER);
  await DummyFactory.createStorage(DUMMY_STORAGE);
  await DummyFactory.createSupplier(DUMMY_SUPPLIER);

  await DummyFactory.createProduct({
    name: 'Baby Beef',
    description: 'Dummy Descriptions',
    current_amount: 0,
    ud_id: 1
  });

  TOK_EDITOR = await DummyFactory.createEditorToken();

  done();
});

describe('Baby Beef', () => {
  test('False Entry - 100', () => {
    return request(MainApp)
      .post(`/entry`)
      .set({ 'Authorization': TOK_EDITOR })
      .send({ ...DUMMY_ENTRY, product_id: 1, supplier_id: 1, amount: 100 })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
      });
  });

  test('Edited Entry - 6.25', () => {
    return request(MainApp)
      .put(`/entry/1`)
      .set({ 'Authorization': TOK_EDITOR })
      .send({ amount: 6.25, product_id: 1 })
      .then(res => {
        expect(res.status).toBe(204);
        expect(res.body).toBeDefined();
      });
  });

  test('Entry - 6.25', () => {
    return request(MainApp)
      .post(`/entry`)
      .set({ 'Authorization': TOK_EDITOR })
      .send({ ...DUMMY_ENTRY, product_id: 1, supplier_id: 1, amount: 6.25 })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
      });
  });

  test('False Exit - 1', () => {
    return request(MainApp)
      .post(`/exit`)
      .set({ 'Authorization': TOK_EDITOR })
      .send({ amount: 1, product_id: 1, worker_id: 1, storage_id: 1 })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
      });
  });

  test('Edited Exit - 2.95', () => {
    return request(MainApp)
      .put(`/exit/3`)
      .set({ 'Authorization': TOK_EDITOR })
      .send({ amount: 2.95, product_id: 1, worker_id: 1, storage_id: 1 })
      .then(res => {
        expect(res.status).toBe(204);
        expect(res.body).toBeDefined();
      });
  });

  test('Exit - 3.00', () => {
    return request(MainApp)
      .post(`/exit`)
      .set({ 'Authorization': TOK_EDITOR })
      .send({ amount: 3, product_id: 1, worker_id: 1, storage_id: 1 })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
      });
  });

  test('Check', () => {
    return request(MainApp)
      .get(`/product/1`)
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();

        expect(Math.round(res.body.current_amount * 100) / 100).toBe(6.55);
      });
  });
});