const request = require('supertest');
const { MainApp } = require('../../src/apps');
const DummyFactory = require('./../setup/DummyFactory');

const { DUMMY_UD } = require('./../setup/Dummy');

let TOK_EDITOR;

beforeAll(async done => {
  TOK_EDITOR = await DummyFactory.createEditorToken();

  done();
});

describe('Route UD', () => {
  test('Create UD', () => {
    return request(MainApp)
      .post('/ud')
      .set({ 'Authorization': TOK_EDITOR })
      .send(DUMMY_UD)
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
      });
  });

  test('Error - Duplicate UD', () => {
    return request(MainApp)
      .post(`/ud`)
      .set({ 'Authorization': TOK_EDITOR })
      .send(DUMMY_UD)
      .then(res => {
        expect(res.status).toBe(400);
        expect(res.body).toBeDefined();
        expect(res.body.error).toBe('UniqueConstraintError');
      });
  });

  test('Get All UDs', () => {
    return request(MainApp)
      .get('/ud')
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body).toHaveLength(1);
      });
  });

  test('Get All Simple', () => {
    return request(MainApp)
      .get('/ud/all/simple')
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body).toHaveLength(1);
        expect(Object.values(res.body[0])).toHaveLength(2);
      });
  });

  test('Get UD by ID', () => {
    return request(MainApp)
      .get('/ud/1')
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();

        expect(res.body.description).toBe(DUMMY_UD.description);
      });
  });

  test('Update', () => {
    return request(MainApp)
      .put('/ud/1')
      .set({ 'Authorization': TOK_EDITOR })
      .send({ description: 'New Description' })
      .then(res => { expect(res.status).toBe(204); });
  });

  describe('Query', () => {
    beforeAll(async done => {
      await Promise.all(
        ['ML', 'L', 'TON'].map(code => DummyFactory.createUd({ code }))
      );
      done();
    });

    test('Query', () => {
      return request(MainApp)
        .get('/ud/query?code=%L%')
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(2);
        });
    });
  });

  describe('Permissions', () => {
    let TOK_VIEWER;

    beforeAll(async done => {
      TOK_VIEWER = await DummyFactory.createViewerToken();
      done();
    });

    describe('Editor Above', () => {
      test('Permission Denied - Viewer - Create UD', () => {
        return request(MainApp)
          .post('/ud').set({ 'Authorization': TOK_VIEWER }).then(res => { expect(res.status).toBe(401); });
      });

      test('Permission Denied - Viewer - Update', () => {
        return request(MainApp)
          .put('/ud/1').set({ 'Authorization': TOK_VIEWER }).then(res => { expect(res.status).toBe(401); });
      });
    });
  });
});


