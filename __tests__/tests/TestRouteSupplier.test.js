const request = require('supertest');
const { MainApp } = require('../../src/apps');
const DummyFactory = require('../setup/DummyFactory');

const { DUMMY_SUPPLIER } = require('../setup/Dummy');
const ROUTE = '/supplier';

let TOK_EDITOR;

beforeAll(async done => {
  TOK_EDITOR = await DummyFactory.createEditorToken();

  done();
});

describe('Route Supplier', () => {
  test('Create Supplier', () => {
    return request(MainApp)
      .post(`${ROUTE}`)
      .set({ 'Authorization': TOK_EDITOR })
      .send(DUMMY_SUPPLIER)
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
      });
  });

  test('Error - Duplicate Supplier', () => {
    return request(MainApp)
      .post(`${ROUTE}`)
      .set({ 'Authorization': TOK_EDITOR })
      .send(DUMMY_SUPPLIER)
      .then(res => {
        expect(res.status).toBe(400);
        expect(res.body).toBeDefined();
        expect(res.body.error).toBe('UniqueConstraintError');
      });
  });

  test('Get All Suppliers', () => {
    return request(MainApp)
      .get(`${ROUTE}`)
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body).toHaveLength(1);
      });
  });

  test('Get All Simple', () => {
    return request(MainApp)
      .get(`${ROUTE}/all/simple`)
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body).toHaveLength(1);
        expect(Object.values(res.body[0])).toHaveLength(2);
      });
  });

  test('Get Supplier by ID', () => {
    return request(MainApp)
      .get(`${ROUTE}/1`)
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body.description).toBe(DUMMY_SUPPLIER.description);
      });
  });

  test('Get All with offset', () => {
    return request(MainApp)
      .get(`${ROUTE}?offset=0`)
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body).toHaveLength(1);
      });
  });

  test('Update', () => {
    return request(MainApp)
      .put(`${ROUTE}/1`)
      .set({ 'Authorization': TOK_EDITOR })
      .send({ description: 'New Description' })
      .then(res => { expect(res.status).toBe(204); });
  });

  describe('Query', () => {
    beforeAll(async done => {
      await Promise.all(
        ['New Supplier', 'New New Supplier', 'Old Supplier'].map(name => DummyFactory.createSupplier({ name }))
      );
      done();
    });

    test('Query', () => {
      return request(MainApp)
        .get(`${ROUTE}/query?name=%New%`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(2);
        });
    });
  });

  describe('Permissions', () => {
    let TOK_VIEWER;

    beforeAll(async done => {
      TOK_VIEWER = await DummyFactory.createViewerToken();
      done();
    });

    describe('Editor Above', () => {
      test('Permission Denied - Viewer - Create', () => {
        return request(MainApp)
          .post(`${ROUTE}`).set({ 'Authorization': TOK_VIEWER }).then(res => { expect(res.status).toBe(401); });
      });

      test('Permission Denied - Viewer - Update', () => {
        return request(MainApp)
          .put(`${ROUTE}/1`).set({ 'Authorization': TOK_VIEWER }).then(res => { expect(res.status).toBe(401); });
      });
    });
  });
});