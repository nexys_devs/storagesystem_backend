const request = require('supertest');
const { MainApp } = require('../../../src/apps');
const DummyFactory = require('../../setup/DummyFactory');

const {
  DUMMY_PRODUCT, DUMMY_UD,
  DUMMY_WORKER, DUMMY_STORAGE, DUMMY_SUPPLIER
} = require('../../setup/Dummy');

const ROUTE = '/product';
const ROUTE_ENTRY = '/entry';
const ROUTE_EXIT = '/exit';
const ROUTE_CAT = '/category';

let TOK_EDITOR, TOK_VIEWER, TOK_ADMIN;

beforeAll(async done => {
  // Tokens
  await DummyFactory.createAdmin();
  TOK_ADMIN = await DummyFactory.getAdminToken();
  TOK_EDITOR = await DummyFactory.createEditorToken();
  TOK_VIEWER = await DummyFactory.createViewerToken();

  // Etc
  await DummyFactory.createWorker(DUMMY_WORKER);
  await DummyFactory.createStorage(DUMMY_STORAGE);
  await DummyFactory.createSupplier(DUMMY_SUPPLIER);
  await DummyFactory.createUd(DUMMY_UD);
  await Promise.all(
    ['Cat1', 'Cat2', 'Cat3'].map(name => DummyFactory.createCategory({ name }))
  );

  await DummyFactory.createProduct(DUMMY_PRODUCT);
  await DummyFactory.addCategoriesToProduct(1, [1]);

  await DummyFactory.createEntry({
    price: 10.90,
    supplier_id: 1,
    amount: 100,
    product_id: 1,
    user_id: 1
  });

  await DummyFactory.createExit({
    worker_id: 1,
    storage_id: 1,
    amount: 10,
    product_id: 1,
    user_id: 1
  });

  done();
});

describe('DELETE - Product', () => {

  describe('Before Deletion', () => {
    test('Get All Products', async done => {
      return request(MainApp)
        .get(`${ROUTE}`)
        .set({ 'Authorization': TOK_ADMIN })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toHaveLength(1);
          done();
        });
    });

    test('Get All Entries by Product', async done => {
      return request(MainApp)
        .get(`${ROUTE_ENTRY}/by/product/1`)
        .set({ 'Authorization': TOK_ADMIN })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toHaveLength(1);
          done();
        });
    });

    test('Get All Exits by Product', async done => {
      return request(MainApp)
        .get(`${ROUTE_EXIT}/by/product/1`)
        .set({ 'Authorization': TOK_ADMIN })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toHaveLength(1);
          done();
        });
    });

    test('Get All Categories', async done => {
      return request(MainApp)
        .get(`${ROUTE_CAT}`)
        .set({ 'Authorization': TOK_ADMIN })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toHaveLength(3);
          done();
        });
    });
  });

  test('Delete Product', async done => {
    return request(MainApp)
      .delete(`${ROUTE}/1`)
      .set({ 'Authorization': TOK_ADMIN })
      .then(res => {
        expect(res.status).toBe(204);
        done();
      });
  });

  describe('After Deletion', () => {
    test('Get All Products', async done => {
      return request(MainApp)
        .get(`${ROUTE}`)
        .set({ 'Authorization': TOK_ADMIN })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toHaveLength(0);
          done();
        });
    });

    test('Get All Entries by Product', async done => {
      return request(MainApp)
        .get(`${ROUTE_ENTRY}/by/product/1`)
        .set({ 'Authorization': TOK_ADMIN })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toHaveLength(0);
          done();
        });
    });

    test('Get All Exits by Product', async done => {
      return request(MainApp)
        .get(`${ROUTE_EXIT}/by/product/1`)
        .set({ 'Authorization': TOK_ADMIN })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toHaveLength(0);
          done();
        });
    });

    test('Get All Categories', async done => {
      return request(MainApp)
        .get(`${ROUTE_CAT}`)
        .set({ 'Authorization': TOK_ADMIN })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toHaveLength(3);
          done();
        });
    });
  });

  describe('Permissions', () => {
    test('Permission Denied - Viewer', async done => {
      return request(MainApp)
        .delete(`${ROUTE}/1`)
        .set({ 'Authorization': TOK_VIEWER })
        .then(res => {
          expect(res.status).toBe(401);
          done();
        });
    });

    test('Permission Denied - Editor', async done => {
      return request(MainApp)
        .delete(`${ROUTE}/1`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(401);
          done();
        });
    });
  });
});
