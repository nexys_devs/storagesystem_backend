const request = require('supertest');
const { MainApp } = require('../../../src/apps');
const DummyFactory = require('../../setup/DummyFactory');

const {
  DUMMY_ENTRY, DUMMY_PRODUCT, DUMMY_UD, DUMMY_SUPPLIER
 } = require('../../setup/Dummy');
const ROUTE = '/entry';

let TOK_EDITOR;

const expectEntryComplete = entry => {
  expect(entry.registry).toBeDefined();
  expect(entry.supplier).toBeDefined();
  expect(entry.registry.product).toBeDefined();
};

beforeAll(async done => {
  await DummyFactory.createUd(DUMMY_UD);
  await DummyFactory.createProduct(DUMMY_PRODUCT);
  await DummyFactory.createSupplier(DUMMY_SUPPLIER);

  TOK_EDITOR = await DummyFactory.createEditorToken();

  done();
});

describe('Route Entry', () => {
  test('Create Entry', () => {
    return request(MainApp)
      .post(`${ROUTE}`)
      .set({ 'Authorization': TOK_EDITOR })
      .send({ ...DUMMY_ENTRY, product_id: 1, supplier_id: 1 })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
      });
  });

  test('Get All Entries', () => {
    return request(MainApp)
      .get(`${ROUTE}`)
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body).toHaveLength(1);
        res.body.forEach(e => expect(e.registry).toBeDefined());
        res.body.forEach(e => expect(e.registry.product).toBeDefined());
      });
  });

  test('Get All with offset', () => {
    return request(MainApp)
      .get(`${ROUTE}?offset=0`)
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body).toHaveLength(1);
      });
  });

  test('Get Entry by ID', () => {
    return request(MainApp)
      .get(`${ROUTE}/1`)
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body.price).toBe(DUMMY_ENTRY.price);
        expect(res.body.registry).toBeDefined();
      });
  });

  test('Get Entry by ID include Product', () => {
    return request(MainApp)
      .get(`${ROUTE}/1/include/product`)
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body.price).toBe(DUMMY_ENTRY.price);
        expect(res.body.registry).toBeDefined();
        expect(res.body.registry.product).toBeDefined();
      });
  });

  describe('Updates', () => {
    test('Update', () => {
      return request(MainApp)
        .put(`${ROUTE}/1`)
        .set({ 'Authorization': TOK_EDITOR })
        .send({ amount: 5, product_id: 1 })
        .then(res => { expect(res.status).toBe(204); });
    });

    test('Check Amount Changed', () => {
      return request(MainApp)
        .get(`/product/1`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body.current_amount).toBe(15); //:: Initial Amount 10 - Entry 10 - Entry Edit 5
        });
    });
  });

  describe('Get By', () => {
    let newSupplier, newUser;

    beforeAll(async done => {
      newUser = await DummyFactory.createAdmin();
      newSupplier = await DummyFactory.createSupplier({ name: 'Second Supplier', company: 'Second Company' });

      let prods = await Promise.all(['Batata Doce', 'Rice'].map(p => (
        DummyFactory.createProduct({ name: p, current_amount: 10, ud_id: 1 })
      )));

      await Promise.all(prods.map(({ product_id }) => (
        DummyFactory.createEntry({
          product_id, supplier_id: newSupplier.supplier_id, user_id: newUser.user_id
        })
      )));

      done();
    });

    test('Get by Product ID', () => {
      return request(MainApp)
        .get(`${ROUTE}/by/product/1`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(1);

          res.body.forEach(e => expectEntryComplete(e));
        });
    });

    test('Get All by User', () => {
      return request(MainApp)
        .get(`${ROUTE}/by/user/${newUser.user_id}`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(2);

          res.body.forEach(e => expectEntryComplete(e));
        });
    });

    test('Get All by Supplier', () => {
      return request(MainApp)
        .get(`${ROUTE}/by/supplier/${newSupplier.supplier_id}`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(2);

          res.body.forEach(e => expectEntryComplete(e));
        });
    });
  });

  describe('Query', () => {

    beforeAll(async done => {
      await DummyFactory.createEntry({
        user_id: 1, product_id: 1, supplier_id: 1, price: 100, amount: 100,
      });

      await DummyFactory.createEntry({
        user_id: 1, product_id: 1, supplier_id: 1, price: 200, amount: 200,
      });

      done();
    });

    test('Filter - ProdcutId', () => {
      return request(MainApp)
        .get(`${ROUTE}/filter?productId=1`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(3);
          res.body.forEach(e => expectEntryComplete(e));
        });
    });

    test('Filter - ProdcutId & Amount Min', () => {
      return request(MainApp)
        .get(`${ROUTE}/filter?productId=1&amountMin=200`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(1);
          res.body.forEach(e => expectEntryComplete(e));
        });
    });

    test('Filter - ProdcutId & Amount Max', () => {
      // Only one Entry - Updated to be 5
      return request(MainApp)
        .get(`${ROUTE}/filter?productId=1&amountMax=10`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(1);
          res.body.forEach(e => expectEntryComplete(e));
        });
    });

    test('Filter - ProdcutId & Price Min', () => {
      return request(MainApp)
        .get(`${ROUTE}/filter?productId=1&priceMin=200`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(1);
          res.body.forEach(e => expectEntryComplete(e));
        });
    });

    test('Filter - ProdcutId & Price Max', () => {
      return request(MainApp)
        .get(`${ROUTE}/filter?productId=1&priceMax=100`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(2);

          res.body.forEach(e => expectEntryComplete(e));
        });
    });

    test('Filter - Time Min', () => {
      return request(MainApp)
        .get(`${ROUTE}/filter?timeMin=2035-01-01`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(0);
        });
    });
  });
});