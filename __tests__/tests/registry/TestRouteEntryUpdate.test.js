const request = require('supertest');
const { MainApp } = require('../../../src/apps');
const DummyFactory = require('../../setup/DummyFactory');

const { DUMMY_UD, DUMMY_SUPPLIER } = require('../../setup/Dummy');
const ROUTE = '/entry';

let TOK_EDITOR;

beforeAll(async done => {
  await DummyFactory.createUd(DUMMY_UD);

  await DummyFactory.createProduct({ name: 'Dummy 1', current_amount: 10, ud_id: 1 });
  await DummyFactory.createProduct({ name: 'Dummy 2', current_amount: 10, ud_id: 1 });

  await DummyFactory.createSupplier(DUMMY_SUPPLIER);

  TOK_EDITOR = await DummyFactory.createEditorToken();

  done();
});

describe('Route Entry - Test Update', () => {
  test('Create Entry', () => {
    return request(MainApp)
      .post(`${ROUTE}`)
      .set({ 'Authorization': TOK_EDITOR })
      .send({ amount: 10, product_id: 1, price: 12.50, supplier_id: 1 })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
      });
  });

  describe('Same Product', () => {
    test('Update - Normal', () => {
      return request(MainApp)
        .put(`${ROUTE}/1`)
        .set({ 'Authorization': TOK_EDITOR })
        .send({ amount: 20, product_id: 1 })
        .then(res => { expect(res.status).toBe(204); });
    });

    test('Check Amount Changed', () => {
      return request(MainApp)
        .get(`/product/1`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body.current_amount).toBe(30);
        });
    });
  });

  describe('Different Product', () => {
    test('Update', () => {
      return request(MainApp)
        .put(`${ROUTE}/1`)
        .set({ 'Authorization': TOK_EDITOR })
        .send({ amount: 20, product_id: 2 })
        .then(res => { expect(res.status).toBe(204); });
    });

    test('Check Amount Changed - P01', () => {
      return request(MainApp)
        .get(`/product/1`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body.current_amount).toBe(10);
        });
    });

    test('Check Amount Changed - P02', () => {
      return request(MainApp)
        .get(`/product/2`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body.current_amount).toBe(30);
        });
    });
  });
});