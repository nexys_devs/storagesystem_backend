const request = require('supertest');
const { MainApp } = require('../../../src/apps');
const DummyFactory = require('../../setup/DummyFactory');

const {
  DUMMY_UD, DUMMY_WORKER, DUMMY_STORAGE
 } = require('../../setup/Dummy');
const ROUTE = '/exit';

let TOK_EDITOR;

beforeAll(async done => {
  await DummyFactory.createUd(DUMMY_UD);
  await DummyFactory.createWorker(DUMMY_WORKER);
  await DummyFactory.createStorage(DUMMY_STORAGE);

  await DummyFactory.createProduct({ name: 'Dummy 1', current_amount: 10, ud_id: 1 });
  await DummyFactory.createProduct({ name: 'Dummy 2', current_amount: 10, ud_id: 1 });
  await DummyFactory.createProduct({ name: 'Dummy 3', current_amount: 0, ud_id: 1 });

  TOK_EDITOR = await DummyFactory.createEditorToken();

  done();
});

describe('Route Entry - Test Update', () => {
  test('Create Entry', () => {
    return request(MainApp)
      .post(`${ROUTE}`)
      .set({ 'Authorization': TOK_EDITOR })
      .send({ amount: 10, product_id: 1, price: 12.50, worker_id: 1, storage_id: 1 })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
      });
  });

  describe('Same Product', () => {
    test('Update - Normal', () => {
      return request(MainApp)
        .put(`${ROUTE}/1`)
        .set({ 'Authorization': TOK_EDITOR })
        .send({ amount: 5, product_id: 1 })
        .then(res => { expect(res.status).toBe(204); });
    });

    test('Check Amount Changed', () => {
      return request(MainApp)
        .get(`/product/1`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body.current_amount).toBe(5);
        });
    });
  });

  describe('Different Product', () => {
    test('Update', () => {
      return request(MainApp)
        .put(`${ROUTE}/1`)
        .set({ 'Authorization': TOK_EDITOR })
        .send({ amount: 5, product_id: 2 })
        .then(res => { expect(res.status).toBe(204); });
    });

    test('Check Amount Changed - P01', () => {
      return request(MainApp)
        .get(`/product/1`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body.current_amount).toBe(10);
        });
    });

    test('Check Amount Changed - P02', () => {
      return request(MainApp)
        .get(`/product/2`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body.current_amount).toBe(5);
        });
    });
  });

  describe('Different Product - Insuficient Amount', () => {
    test('Update - Insuficient Amount', () => {
      return request(MainApp)
        .put(`${ROUTE}/1`)
        .set({ 'Authorization': TOK_EDITOR })
        .send({ amount: 5, product_id: 3 })
        .then(res => { expect(res.status).toBe(409); });
    });
  });
});