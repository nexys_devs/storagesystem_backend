const request = require('supertest');
const { MainApp } = require('../../../src/apps');
const DummyFactory = require('../../setup/DummyFactory');

const {
  DUMMY_PRODUCT, DUMMY_UD, DUMMY_STORAGE, DUMMY_WORKER
} = require('../../setup/Dummy');

const ROUTE = '/exit';
let TOK_EDITOR;

const expectExitComplete = entry => {
  expect(entry.registry).toBeDefined();
  expect(entry.storage).toBeDefined();
  expect(entry.worker).toBeDefined();
  expect(entry.registry.product).toBeDefined();
};

beforeAll(async done => {
  TOK_EDITOR = await DummyFactory.createEditorToken();

  await DummyFactory.createUd(DUMMY_UD);
  await DummyFactory.createWorker(DUMMY_WORKER);
  await DummyFactory.createProduct(DUMMY_PRODUCT);
  await DummyFactory.createStorage(DUMMY_STORAGE);

  done();
});

describe('Route Exit', () => {
  test('Create Exit', () => {
    return request(MainApp)
      .post(`${ROUTE}`)
      .set({ 'Authorization': TOK_EDITOR })
      .send({ amount: 10, product_id: 1, worker_id: 1, storage_id: 1 })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
      });
  });

  test('Get All Entries', () => {
    return request(MainApp)
      .get(`${ROUTE}`)
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body).toHaveLength(1);
        res.body.forEach(e => expect(e.registry).toBeDefined());
        res.body.forEach(e => expect(e.registry.product).toBeDefined());
      });
  });

  test('Get All with offset', () => {
    return request(MainApp)
      .get(`${ROUTE}?offset=0`)
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body).toHaveLength(1);
      });
  });

  test('Get Entry by ID', () => {
    return request(MainApp)
      .get(`${ROUTE}/1`)
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body.exit_id).toBe(1);
        expect(res.body.registry).toBeDefined();
      });
  });

  test('Get Entry by ID include Product', () => {
    return request(MainApp)
      .get(`${ROUTE}/1/include/product`)
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body.exit_id).toBe(1);

        expectExitComplete(res.body);
      });
  });

  describe('Updates', () => {
    test('Update', () => {
      return request(MainApp)
        .put(`${ROUTE}/1`)
        .set({ 'Authorization': TOK_EDITOR })
        .send({ amount: 5, product_id: 1 })
        .then(res => { expect(res.status).toBe(204); });
    });

    test('Check Amount Changed', () => {
      return request(MainApp)
        .get(`/product/1`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body.current_amount).toBe(5); //:: Initial Amount 10 - Exit 10 - Exit Edit 5
        });
    });
  });

  describe('Get By', () => {
    let newStorage, newWorker, newUser;

    beforeAll(async done => {
      newUser = await DummyFactory.createAdmin();
      newStorage = await DummyFactory.createStorage({ name: 'Second Storage' });
      newWorker = await DummyFactory.createWorker({ name: 'Second Worker ' });

      let prods = await Promise.all(['Batata Doce', 'Rice'].map(p => (
        DummyFactory.createProduct({ name: p, current_amount: 10, ud_id: 1 })
      )));

      await Promise.all(prods.map(({ product_id }) => (
        DummyFactory.createExit({
          product_id,
          storage_id: newStorage.storage_id,
          worker_id: newWorker.worker_id,
          user_id: newUser.user_id,
          amount: 1
        })
      )));

      done();
    });

    test('Get All by Product', () => {
      return request(MainApp)
        .get(`${ROUTE}/by/product/1`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(1);

          res.body.forEach(e => expectExitComplete(e));
        });
    });

    test('Get All by User', () => {
      return request(MainApp)
        .get(`${ROUTE}/by/user/${newUser.user_id}`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(2);

          res.body.forEach(e => expectExitComplete(e));
        });
    });

    test('Get All by Worker', () => {
      return request(MainApp)
        .get(`${ROUTE}/by/worker/${newWorker.worker_id}`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(2);

          res.body.forEach(e => expectExitComplete(e));
        });
    });

    test('Get All by Storage', () => {
      return request(MainApp)
        .get(`${ROUTE}/by/storage/${newStorage.storage_id}`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(2);

          res.body.forEach(e => expectExitComplete(e));
        });
    });
  });

  describe('Query', () => {

    test('Filter - ProdcutId', () => {
      return request(MainApp)
        .get(`${ROUTE}/filter?productId=1`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(1);

          res.body.forEach(e => expectExitComplete(e));
        });
    });

    test('Filter - ProdcutName', () => {
      return request(MainApp)
        .get(`${ROUTE}/filter?productName=%Dummy%`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(1);

          res.body.forEach(e => expectExitComplete(e));
        });
    });

    test('Filter - StorageId', () => {
      return request(MainApp)
        .get(`${ROUTE}/filter?storageId=2`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(2);

          res.body.forEach(e => expectExitComplete(e));
        });
    });

    test('Filter - StorageName', () => {
      return request(MainApp)
        .get(`${ROUTE}/filter?storageName=%Dummy%`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(1);

          res.body.forEach(e => expectExitComplete(e));
        });
    });

    test('Filter - WorkerId', () => {
      return request(MainApp)
        .get(`${ROUTE}/filter?workerId=2`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(2);

          res.body.forEach(e => expectExitComplete(e));
        });
    });

    test('Filter - WorkerName', () => {
      return request(MainApp)
        .get(`${ROUTE}/filter?workerName=%Second%`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(2);

          res.body.forEach(e => expectExitComplete(e));
        });
    });

    test('Filter - Amount', () => {
      return request(MainApp)
        .get(`${ROUTE}/filter?amountMin=0&amountMax=3`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(2);

          res.body.forEach(e => expectExitComplete(e));
        });
    });

    test('Filter - Time', () => {
      return request(MainApp)
        .get(`${ROUTE}/filter?timeMin=2035-01-01`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(0);
        });
    });
  });
});