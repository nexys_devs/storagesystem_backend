const request = require('supertest');
const { MainApp } = require('../../src/apps');
const DummyFactory = require('../setup/DummyFactory');

const { DUMMY_UD, DUMMY_SUPPLIER, DUMMY_WORKER, DUMMY_STORAGE } = require('../setup/Dummy');
const ROUTE = '/report';

let TOK_EDITOR;

beforeAll(async done => {
  await DummyFactory.createUd(DUMMY_UD);
  await DummyFactory.createWorker(DUMMY_WORKER);
  await DummyFactory.createStorage(DUMMY_STORAGE);
  await DummyFactory.createSupplier(DUMMY_SUPPLIER);

  TOK_EDITOR = await DummyFactory.createEditorToken();

  await Promise.all(['Category A', 'Category B'].map(c => DummyFactory.createCategory({ name: c })));

  await Promise.all(
    [
      { name: 'Normal', current_amount: 100, warning_amount: 10, danger_amount: 5, ud_id: 1 },  // Ok
      { name: 'Warning', current_amount: 20, warning_amount: 20, danger_amount: 5, ud_id: 1 },  // Warn
      { name: 'Warning2', current_amount: 8, warning_amount: 10, danger_amount: 5, ud_id: 1 },  // Warn
      { name: 'Danger', current_amount: 5, warning_amount: 10, danger_amount: 5, ud_id: 1 },    // Danger
      { name: 'Danger2', current_amount: 2, warning_amount: 10, danger_amount: 5, ud_id: 1 }   // Danger
    ].map(p => DummyFactory.createProduct(p))
  );

  await DummyFactory.addCategoriesToProduct(2, [1]);

  // For Product value the last entry will be considered.
  // For product 1 the amoutn will be 100 at a price of 20.
  const entries = [
    { product_id: 1, amount: 90, price: 100 },
    { product_id: 1, amount: 10, price: 20 },
    { product_id: 2, amount: 20, price: 40 }
  ];

  for (const { product_id, amount, price } of entries) {
    await DummyFactory.createEntry({
      price, amount, product_id, supplier_id: 1, user_id: 1
    });

    await new Promise(resolve => setTimeout(resolve, 1000));
  }

  done();
});

describe('Route Report', () => {

  describe('Shop List', () => {
    test('Shop List - All Leves', () => {
      return request(MainApp)
        .get(`${ROUTE}/shoplist`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(4);

          res.body.forEach(p => {
            expect(p.ud).toBeDefined();
            expect(p.categories).toBeDefined();
          });
        });
    });

    test('Shop List - Warning', () => {
      return request(MainApp)
        .get(`${ROUTE}/shoplist?level=WARN`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(2);

          res.body.forEach(p => {
            expect(p.ud).toBeDefined();
            expect(p.categories).toBeDefined();
          });
        });
    });

    test('Shop List - Danger', () => {
      return request(MainApp)
        .get(`${ROUTE}/shoplist?level=DANGER`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(2);

          res.body.forEach(p => {
            expect(p.ud).toBeDefined();
            expect(p.categories).toBeDefined();
          });
        });
    });
  });

  describe('Storage Value', () => {
    test('Storage Value', () => {
      return request(MainApp)
        .get(`${ROUTE}/storageValue`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(2);

          const productOne = res.body.find(p => p.product.product_id === 1);
          const productTwo = res.body.find(p => p.product.product_id === 2);

          expect(productOne.totalValue).toBe(200);
          expect(productOne.valueByUnit).toBe(2);

          expect(productTwo.valueByUnit).toBe(2);
          expect(productTwo.totalValue).toBe(40);
        });
    });

    test('Storage Value - By Category', () => {
      return request(MainApp)
        .get(`${ROUTE}/storageValue?divideByCategory=true`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(2);

          expect(res.body[0].category).toBeDefined();
          expect(res.body[0].products).toHaveLength(1);
          expect(res.body[0].totalValue).toBe(40);

          expect(res.body[1].category).toBe(null);
          expect(res.body[1].products).toHaveLength(1);
          expect(res.body[1].totalValue).toBe(200);
        });
    });

    test('Incorrect Query Param', () => {
      return request(MainApp)
        .get(`${ROUTE}/storageValue?divideByCategory=aaa`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(400);
        });
    });

    describe('Filter', () => {
      test('Filter By Name', () => {
        return request(MainApp)
          .get(`${ROUTE}/storageValue?name=Norm%`)
          .set({ 'Authorization': TOK_EDITOR })
          .then(res => {
            expect(res.status).toBe(200);
            expect(res.body).toBeDefined();
            expect(res.body).toHaveLength(1);

            expect(res.body[0].totalValue).toBe(200);
            expect(res.body[0].valueByUnit).toBe(2);
          });
      });

      test('Filter By MaxValue', () => {
        return request(MainApp)
          .get(`${ROUTE}/storageValue?maxValue=40`)
          .set({ 'Authorization': TOK_EDITOR })
          .then(res => {
            expect(res.status).toBe(200);
            expect(res.body).toBeDefined();
            expect(res.body).toHaveLength(1);

            expect(res.body[0].totalValue).toBe(40);
            expect(res.body[0].valueByUnit).toBe(2);
          });
      });


      test('Filter By MinValue', () => {
        return request(MainApp)
          .get(`${ROUTE}/storageValue?minValue=41`)
          .set({ 'Authorization': TOK_EDITOR })
          .then(res => {
            expect(res.status).toBe(200);
            expect(res.body).toBeDefined();
            expect(res.body).toHaveLength(1);

            expect(res.body[0].totalValue).toBe(200);
            expect(res.body[0].valueByUnit).toBe(2);
          });
      });

      test('Filter By Category Id', () => {
        return request(MainApp)
          .get(`${ROUTE}/storageValue?category_id=1&divideByCategory=true`)
          .set({ 'Authorization': TOK_EDITOR })
          .then(res => {
            expect(res.status).toBe(200);
            expect(res.body).toBeDefined();
            expect(res.body).toHaveLength(1);
          });
      });
    });

    describe('Same Storage Value After Product Exit', () => {
      //:: Removing 10 from Product 1
      test('Removing Product', () => {
        return request(MainApp)
          .post(`/exit`)
          .set({ 'Authorization': TOK_EDITOR })
          .send({ amount: 10, product_id: 1, worker_id: 1, storage_id: 1 })
          .then(res => {
            expect(res.status).toBe(200);
            expect(res.body).toBeDefined();
          });
      });

      test('Value After Removal', () => {
        return request(MainApp)
          .get(`${ROUTE}/storageValue`)
          .set({ 'Authorization': TOK_EDITOR })
          .then(res => {
            expect(res.status).toBe(200);
            expect(res.body).toBeDefined();
            expect(res.body).toHaveLength(2);

            expect(res.body[0].totalValue).toBe(180);
            expect(res.body[0].valueByUnit).toBe(2);

            expect(res.body[1].totalValue).toBe(40);
            expect(res.body[1].valueByUnit).toBe(2);
          });
      });
    });
  });
});
