const request = require('supertest');
const { MainApp } = require('../../src/apps');
const DummyFactory = require('../setup/DummyFactory');

const ROUTE_SUPLIER = '/supplier';

let TOK_EDITOR;

beforeAll(async done => {
  TOK_EDITOR = await DummyFactory.createEditorToken();
  done();
});

describe('Validation Test', () => {

  describe('Supplier', () => {

    test('Accent on Name - Route Supplier', () => {
      return request(MainApp)
        .post(`${ROUTE_SUPLIER}`)
        .set({ 'Authorization': TOK_EDITOR })
        .send({
          name: 'Meu nome é jonas',
          company: 'Compania',
          email: 'fakemail@gmail.com',
          phone: '',
          address: '',
          description: '',
        })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
        });
    });

    test('Not Trimed email - Route Supplier', () => {
      return request(MainApp)
        .post(`${ROUTE_SUPLIER}`)
        .set({ 'Authorization': TOK_EDITOR })
        .send({
          name: 'Jonas',
          company: 'Compania',
          email: '     fakemail@gmail.com        ',
          phone: '',
          address: '',
          description: '',
        })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
        });
    });
  });
});