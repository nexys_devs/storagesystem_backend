const request = require('supertest');
const { MainApp } = require('../../src/apps');
const DummyFactory = require('../setup/DummyFactory');

const { DUMMY_USER_VIEWER } = require('../setup/Dummy');

let TOK_ADMIN;

beforeAll(async done => {
  await DummyFactory.createAdmin();
  TOK_ADMIN = await DummyFactory.getAdminToken();

  done();
});

describe('Route User', () => {
  test('Create User', () => {
    return request(MainApp)
      .post('/user')
      .set({ 'Authorization': TOK_ADMIN })
      .send(DUMMY_USER_VIEWER)
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
      });
  });

  test('Error - Duplicate UD', () => {
    return request(MainApp)
      .post('/user')
      .set({ 'Authorization': TOK_ADMIN })
      .send(DUMMY_USER_VIEWER)
      .then(res => {
        expect(res.status).toBe(400);
        expect(res.body).toBeDefined();
        expect(res.body.error).toBe('UniqueConstraintError');
      });
  });

  test('Get All Users', () => {
    return request(MainApp)
      .get('/user')
      .set({ 'Authorization': TOK_ADMIN })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body).toHaveLength(2);
      });
  });

  test('Get User by ID', () => {
    return request(MainApp)
      .get('/user/2')
      .set({ 'Authorization': TOK_ADMIN })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body.login).toBe(DUMMY_USER_VIEWER.login);
      });
  });

  test('Update', () => {
    return request(MainApp)
      .put('/user/2')
      .set({ 'Authorization': TOK_ADMIN })
      .send({ first_name: 'New Name' })
      .then(res => { expect(res.status).toBe(204); });
  });

  test('Set Role', () => {
    return request(MainApp)
      .put('/user/2/setRole/2')
      .set({ 'Authorization': TOK_ADMIN })
      .then(res => { expect(res.status).toBe(204); });
  });

  test('Check Role', () => {
    return request(MainApp)
      .get('/user/2')
      .set({ 'Authorization': TOK_ADMIN })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body.user_role_id).toBe(2);
      });
  });

  describe('Permissions', () => {
    let TOK_EDITOR; // Editor - User had Role modified on Test `Set Role`

    beforeAll(async done => {
      TOK_EDITOR = await DummyFactory.getLoginToken(DUMMY_USER_VIEWER.login, DUMMY_USER_VIEWER.password);
      done();
    });

    test('Permission Denied - Editor - Create User', () => {
      return request(MainApp)
        .post('/user').set({ 'Authorization': TOK_EDITOR }).then(res => { expect(res.status).toBe(401); });
    });

    test('Permission Denied - Editor - Get All Users', () => {
      return request(MainApp)
        .get('/user').set({ 'Authorization': TOK_EDITOR }).then(res => { expect(res.status).toBe(401); });
    });

    test('Permission Denied - Editor - Get User by ID', () => {
      return request(MainApp)
        .get('/user/2').set({ 'Authorization': TOK_EDITOR }).then(res => { expect(res.status).toBe(401); });
    });

    test('Permission Denied - Editor - Update', () => {
      return request(MainApp)
        .put('/user/2').set({ 'Authorization': TOK_EDITOR }).then(res => { expect(res.status).toBe(401); });
    });

    test('Permission Denied - Editor - Set Role', () => {
      return request(MainApp)
        .put('/user/2/setRole/2').set({ 'Authorization': TOK_EDITOR }).then(res => { expect(res.status).toBe(401); });
    });
  });
});