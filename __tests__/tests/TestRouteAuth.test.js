const request = require('supertest');
const { MainApp } = require('../../src/apps');
const DummyFactory = require('./../setup/DummyFactory');

const ROUTE_LOGIN = '/auth/login';

beforeAll(async done => {
  await DummyFactory.createAdmin();
  done();
});

describe('Route Auth', () => {
  test('Login', () => {
    return request(MainApp)
      .post(ROUTE_LOGIN)
      .send({ login: 'Admin', password: '%secret_pasword%' })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
      });
  });

  test('Wrong Password', () => {
    return request(MainApp)
      .post(ROUTE_LOGIN)
      .send({ login: 'Admin', password: 'wrong' })
      .then(res => expect(res.status).toBe(400));
  });

  test('Undefined Password', () => {
    return request(MainApp)
      .post(ROUTE_LOGIN)
      .send({ login: 'Admin' })
      .then(res => expect(res.status).toBe(400));
  });

  test('Undefined User', () => {
    return request(MainApp)
      .post(ROUTE_LOGIN)
      .send({ password: 'wrong' })
      .then(res => expect(res.status).toBe(400));
  });
});