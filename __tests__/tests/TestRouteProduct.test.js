const request = require('supertest');
const { MainApp } = require('../../src/apps');
const DummyFactory = require('../setup/DummyFactory');

const { DUMMY_PRODUCT, DUMMY_UD, DUMMY_ENTRY, DUMMY_SUPPLIER } = require('../setup/Dummy');

const ROUTE = '/product';
const ROUTE_ENTRY = '/entry';

let TOK_EDITOR;

beforeAll(async done => {
  await DummyFactory.createUd(DUMMY_UD);
  await DummyFactory.createSupplier(DUMMY_SUPPLIER);

  await Promise.all(
    ['Cat1', 'Cat2', 'Cat3'].map(name => DummyFactory.createCategory({ name }))
  );

  TOK_EDITOR = await DummyFactory.createEditorToken();

  done();
});

describe('Route Product', () => {
  test('Create Product', () => {
    return request(MainApp)
      .post(`${ROUTE}`)
      .set({ 'Authorization': TOK_EDITOR })
      .send({ ...DUMMY_PRODUCT, categories: [1, 2] })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body.categories).toBeDefined();
        expect(res.body.categories).toHaveLength(2);
        expect(res.body.current_amount).toBe(0);
      });
  });

  test('Create Entry', () => {
    return request(MainApp)
      .post(`${ROUTE_ENTRY}`)
      .set({ 'Authorization': TOK_EDITOR })
      .send({ ...DUMMY_ENTRY, product_id: 1, supplier_id: 1 })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
      });
  });

  test('Error - Duplicate Product', () => {
    return request(MainApp)
      .post(`${ROUTE}`)
      .set({ 'Authorization': TOK_EDITOR })
      .send(DUMMY_PRODUCT)
      .then(res => {
        expect(res.status).toBe(400);
        expect(res.body).toBeDefined();
        expect(res.body.error).toBe('UniqueConstraintError');
      });
  });

  test('Get All Product', () => {
    return request(MainApp)
      .get(`${ROUTE}`)
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body).toHaveLength(1);

        res.body.forEach(p => {
          expect(p.categories).toBeDefined();
        });
      });
  });

  test('Get All Simple', () => {
    return request(MainApp)
      .get(`${ROUTE}/all/simple`)
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body).toHaveLength(1);
        expect(Object.values(res.body[0])).toHaveLength(2);
      });
  });

  test('Get Product by ID', () => {
    return request(MainApp)
      .get(`${ROUTE}/1`)
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body.description).toBe(DUMMY_PRODUCT.description);
        expect(res.body.ud).toBeDefined();
        expect(res.body.categories).toBeDefined();
      });
  });

  test('Get All with offset', () => {
    return request(MainApp)
      .get(`${ROUTE}?offset=0`)
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body).toHaveLength(1);
      });
  });

  describe('Update', () => {
    test('Update', () => {
      return request(MainApp)
        .put(`${ROUTE}/1`)
        .set({ 'Authorization': TOK_EDITOR })
        .send({ name: 'Tomate', categories: [3] })
        .then(res => { expect(res.status).toBe(204); });
    });

    test('Update - Unexisting Category', () => {
      return request(MainApp)
        .put(`${ROUTE}/1`)
        .set({ 'Authorization': TOK_EDITOR })
        .send({ name: 'Tomate', categories: [12] })
        .then(res => {
          expect(res.status).toBe(400);
          expect(res.body).toBeDefined();
          expect(res.body.error).toBe('ConstraintError');
        });
    });

    test('Verify Category Set after UPDATE', () => {
      return request(MainApp)
        .get(`${ROUTE}/1`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body.name).toBe('Tomate');
          expect(res.body.ud).toBeDefined();
          expect(res.body.categories).toBeDefined();
          expect(res.body.categories).toHaveLength(1);
          expect(res.body.categories[0].name).toBe('Cat3');
        });
    });
  });

  describe('Categories', () => {

    test('Category Add', () => {
      return request(MainApp)
        .post(`${ROUTE}/1/category/add`)
        .set({ 'Authorization': TOK_EDITOR })
        .send([1, 2])
        .then(res => { expect(res.status).toBe(204); });
    });

    test('Category Remove', () => {
      return request(MainApp)
        .post(`${ROUTE}/1/category/remove`)
        .set({ 'Authorization': TOK_EDITOR })
        .send([1])
        .then(res => { expect(res.status).toBe(204); });
    });

    test('Category Set', () => {
      return request(MainApp)
        .post(`${ROUTE}/1/category/set`)
        .set({ 'Authorization': TOK_EDITOR })
        .send([3])
        .then(res => { expect(res.status).toBe(204); });
    });

  });

  describe('Query', () => {
    beforeAll(async done => {
      await Promise.all(
        ['Batata', 'Batata Doce', 'Picles'].map(name => DummyFactory.createProduct({ name, ud_id: 1 }))
      );
      done();
    });

    test('Query - Lower Case', () => {
      return request(MainApp)
        .get(`${ROUTE}/query?name=%tata%`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(2);
        });
    });

    test('Query - Upper Case', () => {
      return request(MainApp)
        .get(`${ROUTE}/query?name=%TaTa%`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(2);
        });
    });
  });

  describe('Permissions', () => {
    let TOK_VIEWER;

    beforeAll(async done => {
      TOK_VIEWER = await DummyFactory.createViewerToken();
      done();
    });

    describe('Editor Above', () => {
      test('Permission Denied - Viewer - Create', () => {
        return request(MainApp)
          .post(`${ROUTE}`).set({ 'Authorization': TOK_VIEWER }).then(res => { expect(res.status).toBe(401); });
      });

      test('Permission Denied - Viewer - Update', () => {
        return request(MainApp)
          .put(`${ROUTE}/1`).set({ 'Authorization': TOK_VIEWER }).then(res => { expect(res.status).toBe(401); });
      });

      test('Permission Denied - Viewer - Category Add', () => {
        return request(MainApp)
          .post(`${ROUTE}/1/category/add`).set({ 'Authorization': TOK_VIEWER }).then(res => { expect(res.status).toBe(401); });
      });

      test('Permission Denied - Viewer - Category Remove', () => {
        return request(MainApp)
          .post(`${ROUTE}/1/category/remove`).set({ 'Authorization': TOK_VIEWER }).then(res => { expect(res.status).toBe(401); });
      });

      test('Permission Denied - Viewer - Category Set', () => {
        return request(MainApp)
          .post(`${ROUTE}/1/category/set`).set({ 'Authorization': TOK_VIEWER }).then(res => { expect(res.status).toBe(401); });
      });
    });
  });
});