const request = require('supertest');
const { MainApp } = require('../../src/apps');
const DummyFactory = require('../setup/DummyFactory');

const { DUMMY_CATEGORY } = require('../setup/Dummy');
const ROUTE = '/category';

let TOK_EDITOR;

beforeAll(async done => {
  TOK_EDITOR = await DummyFactory.createEditorToken();

  done();
});

describe('Route Category', () => {
  test('Create Category', () => {
    return request(MainApp)
      .post(`${ROUTE}`)
      .set({ 'Authorization': TOK_EDITOR })
      .send(DUMMY_CATEGORY)
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
      });
  });

  test('Error - Duplicate Category', () => {
    return request(MainApp)
      .post(`${ROUTE}`)
      .set({ 'Authorization': TOK_EDITOR })
      .send(DUMMY_CATEGORY)
      .then(res => {
        expect(res.status).toBe(400);
        expect(res.body).toBeDefined();
        expect(res.body.error).toBe('UniqueConstraintError');
      });
  });

  test('Get All Categorys', () => {
    return request(MainApp)
      .get(`${ROUTE}`)
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body).toHaveLength(1);
      });
  });

  test('Get All Simple', () => {
    return request(MainApp)
      .get(`${ROUTE}/all/simple`)
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body).toHaveLength(1);
        expect(Object.values(res.body[0])).toHaveLength(2);
      });
  });

  test('Get Category by ID', () => {
    return request(MainApp)
      .get(`${ROUTE}/1`)
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body.description).toBe(DUMMY_CATEGORY.description);
      });
  });

  test('Get All with offset', () => {
    return request(MainApp)
      .get(`${ROUTE}?offset=0`)
      .set({ 'Authorization': TOK_EDITOR })
      .then(res => {
        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
        expect(res.body).toHaveLength(1);
      });
  });

  test('Update', () => {
    return request(MainApp)
      .put(`${ROUTE}/1`)
      .set({ 'Authorization': TOK_EDITOR })
      .send({ description: 'New Description' })
      .then(res => { expect(res.status).toBe(204); });
  });

  describe('Query', () => {
    beforeAll(async done => {
      await Promise.all(
        ['Nova Categoria', 'Novas Categorias', 'Velha'].map(name => DummyFactory.createCategory({ name }))
      );
      done();
    });

    test('Query', () => {
      return request(MainApp)
        .get(`${ROUTE}/query?name=%Nova%`)
        .set({ 'Authorization': TOK_EDITOR })
        .then(res => {
          expect(res.status).toBe(200);
          expect(res.body).toBeDefined();
          expect(res.body).toHaveLength(2);
        });
    });
  });

  describe('Permissions', () => {
    let TOK_VIEWER;

    beforeAll(async done => {
      TOK_VIEWER = await DummyFactory.createViewerToken();
      done();
    });

    describe('Editor Above', () => {
      test('Permission Denied - Viewer - Create Category', () => {
        return request(MainApp)
          .post(`${ROUTE}`).set({ 'Authorization': TOK_VIEWER }).then(res => { expect(res.status).toBe(401); });
      });

      test('Permission Denied - Viewer - Update', () => {
        return request(MainApp)
          .put(`${ROUTE}/1`).set({ 'Authorization': TOK_VIEWER }).then(res => { expect(res.status).toBe(401); });
      });
    });
  });
});