module.exports.DUMMY_ADMIN_USER = {
  user_id: 1,
  login: 'Admin',
  first_name: 'Admin',
  last_name: 'Admin',
  email: 'admin@boss.com',
  user_role_id: 1,
  password: '%secret_pasword%'
};

module.exports.DUMMY_USER_EDITOR = {
  login: 'Test_User_Editor',
  first_name: 'Editor',
  last_name: 'Test',
  email: 'editor@test.com',
  phone: '999999999',
  birth: '2020-01-15',
  password: '12345',
  user_role_id: 2,
};

module.exports.DUMMY_USER_VIEWER = {
  login: 'Test_User_Viewer',
  first_name: 'Viewer',
  last_name: 'Test',
  email: 'viewer@test.com',
  phone: '999999999',
  birth: '2020-01-15',
  password: '12345',
  user_role_id: 3,
};

module.exports.DUMMY_UD = {
  code: 'KG',
  description: 'Kilo'
};

module.exports.DUMMY_CATEGORY = {
  name: 'Bebidas',
  description: 'Bebidas'
};

module.exports.DUMMY_SUPPLIER = {
  name: 'Dummy Supplier',
  company: 'Dummy Company',
  description: 'Supplier muito bacana'
};

module.exports.DUMMY_WORKER = {
  name: 'Dummy Worker'
};

module.exports.DUMMY_STORAGE = {
  name: 'Dummy Storage',
  description: 'Dummy Description'
};

module.exports.DUMMY_PRODUCT = {
  name: 'Dummy Product',
  description: 'Dummy Descriptions',
  current_amount: 10,
  ud_id: 1
};

module.exports.DUMMY_ENTRY = {
  price: 12.50,
  amount: 10,
};

module.exports.DUMMY_EXIT = {
  amount: 10,
};