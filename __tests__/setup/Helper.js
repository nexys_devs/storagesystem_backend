const request = require('supertest');
const { MainApp } = require('../../src/apps');

module.exports = {
  getLoginToken: (login, password) => {
    return new Promise((resolve, reject) => {
      request(MainApp)
        .post('/auth/login')
        .send({ login, password })
        .then(res => {
          if (res.status !== 200) { reject(res); }

          resolve(res.headers.authorization);
        });
    });
  }
};