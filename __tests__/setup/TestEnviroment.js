const NodeEnvironment = require('jest-environment-node');
const Models = require('./../../src/models');
const { Logger } = require('../../src/utils');

const { TEARDOWN, DB_DIALECT } = process.env;

const { runSqlScript } = require('./../../src/models/utils');
const DB_SQL_SETUP = process.cwd() + '/scripts/DatabaseSetup.sql';

class TestEnviroment extends NodeEnvironment {
  constructor(config, context) {
    super(config, context);

    this.shouldTeardown = (TEARDOWN || '').toUpperCase() === 'TRUE';
    this.isPostgres = (DB_DIALECT || '').toUpperCase() === 'POSTGRES';
  }

  async setup() {
    await super.setup();
  }

  async teardown() {
    if (!this.shouldTeardown) {
      Logger.warn('Test Enviroment Tear Down - SKIPPING');
      return;
    }

    try {
      console.log('Test Enviroment Tear Down');
      // Remove Foreign key Checks

      // Truncate All Tables - Models to ignore list
      const IGNORE = [
        'Sequelize',
        'sequelize',
        'UserRole'
      ];

      let models = Object.keys(Models).filter(k => !IGNORE.includes(k) && Models[k].destroy).map(k => Models[k]);

      if (this.isPostgres)
        return await this.truncatePostgres();

      return await this.truncateSql(models);

    } catch (err) {
      Logger.error('!-- TearDown Error --!', err);
      throw err;
    }
  }

  async truncatePostgres() {
    await Models.sequelize.sync({ force: true });
    await runSqlScript(Models, DB_SQL_SETUP);
  }

  async truncateSql(models) {
    await Models.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, { raw: true });

    for (let m of models)
      await m.destroy({ truncate: true, force: true, cascade: false });

    await Models.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, { raw: true });
  }
}

module.exports = TestEnviroment;