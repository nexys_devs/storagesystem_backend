const request = require('supertest');
const { MainApp } = require('../../src/apps');

const {
  DaoUser, DaoUserAuth, DaoUD, DaoCategory, DaoSupplier,
  DaoWorker, DaoStorage, DaoProduct, DaoProductCategory,
  DaoEntry,
  DaoExit
} = require('./../../src/dao');
const { DUMMY_ADMIN_USER, DUMMY_USER_VIEWER, DUMMY_USER_EDITOR } = require('./Dummy');

// 12345 - $2a$10$m92ztytdButOoIVo88uoQu0XDoso/D/3gx2VyqRtX942HWr.x60sG
const HASH = '$2a$10$m92ztytdButOoIVo88uoQu0XDoso/D/3gx2VyqRtX942HWr.x60sG';

class DummyFactory {
  constructor() {
    this._daoUser = new DaoUser();
    this._daoUserAuth = new DaoUserAuth();
    this._daoUd = new DaoUD();
    this._daoCategory = new DaoCategory();
    this._daoSupplier = new DaoSupplier();
    this._daoWorker = new DaoWorker();
    this._daoStorage = new DaoStorage();
    this._daoProduct = new DaoProduct();
    this._daoProductCategory = new DaoProductCategory();
    this._daoEntry = new DaoEntry();
    this._daoExit = new DaoExit();
  }

  createAdmin() {
    return this._createUser(DUMMY_ADMIN_USER, '$2a$10$UvwUPSdhnLl22jriwTs0zuSVRF.8lLxcJP9aA/II3wcgouYZpKDky');
  }

  createUserEditor() {
    return this._createUser(DUMMY_USER_EDITOR, HASH);
  }

  createUserViewer() {
    return this._createUser(DUMMY_USER_VIEWER, HASH);
  }

  createUd(ud) {
    return this._daoUd.create(ud);
  }

  createCategory(cat) {
    return this._daoCategory.create(cat);
  }

  createSupplier(sup) {
    return this._daoSupplier.create(sup);
  }

  createWorker(w) {
    return this._daoWorker.create(w);
  }

  createStorage(s) {
    return this._daoStorage.create(s);
  }

  createProduct(p) {
    return this._daoProduct.create(p);
  }

  createEntry(e) {
    return this._daoEntry.create(e);
  }

  createExit(e) {
    return this._daoExit.create(e);
  }

  // Category
  addCategoriesToProduct(p, c) {
    return this._daoProductCategory.addCategoriesToProduct(p, c);
  }

  // Helps
  async _createUser(user, secret) {
    let u = await this._daoUser.create(user);
    await this._daoUserAuth.create({ user_id: u.user_id, secret });

    return u;
  }

  // Login
  async createViewerToken() {
    try {
      await this.createUserViewer();
      return await this.getLoginToken(DUMMY_USER_VIEWER.login, DUMMY_USER_VIEWER.password);
    } catch (err) {
      console.error(err);
      throw err;
    }
  }

  async createEditorToken() {
    try {
      await this.createUserEditor();
      return await this.getLoginToken(DUMMY_USER_EDITOR.login, DUMMY_USER_EDITOR.password);
    } catch (err) {
      console.error(err);
      throw err;
    }
  }

  getAdminToken() {
    return this.getLoginToken(DUMMY_ADMIN_USER.login, DUMMY_ADMIN_USER.password);
  }

  getLoginToken(login, password) {
    return new Promise((resolve, reject) => {
      request(MainApp)
        .post('/auth/login')
        .send({ login, password })
        .then(res => {
          if (res.status !== 200) { reject(res); }

          resolve(res.headers.authorization);
        });
    });
  }
}

const dummyFactory = new DummyFactory();
module.exports = dummyFactory;