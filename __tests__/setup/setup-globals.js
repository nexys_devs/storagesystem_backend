require('dotenv').config({ path: process.cwd() + '/__tests__/setup/test.env' });

const Models = require('./../../src/models');
const { Logger } = require('../../src/utils');
const { runSqlScript } = require('./../../src/models/utils');

const { DATABASE_URL, TEARDOWN } = process.env;

if (!DATABASE_URL.includes('test')) {
  throw new Error('!-- NOT USING A TEST DATABASE --!');
}

if (!(DATABASE_URL || '').includes('localhost')) {
  throw new Error('!-- NOT USING LOCALHOST DATABASE --!');
}

if (TEARDOWN !== 'TRUE')
  Logger.warn('\nTest Teardown not enable.');

const DB_SQL_SETUP = process.cwd() + '/scripts/DatabaseSetup.sql';

module.exports = async function () {
  try {
    console.log('\nGlobal Setup - Start');

    console.log(`\tStarting Clean DataBase - ${DATABASE_URL}`);
    await Models.sequelize.sync({ force: true })
      .catch(err => console.error(err));

    console.log('\tRunning Scripts');
    await runSqlScript(Models, DB_SQL_SETUP);

    console.log('Global Setup - End\n');
  } catch (err) {
    Logger.error('!-- GLOBAL TEST SET UP FAILED --!', err);
    process.exit(-9);
  }
};