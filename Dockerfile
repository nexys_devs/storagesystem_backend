FROM node:14.19.0-alpine3.14
WORKDIR /usr/src/app/detroid

COPY . .

COPY package*.json ./

RUN npm ci --only=production

CMD [ "node", "./src/server.js" ]
